package dev.tacon.common.time;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.junit.jupiter.api.Test;

public class DateTimeUtilsTest {

	@Test
	public void testToOffsetDateTimeFromUTC() {
		final LocalDateTime dateTime = LocalDateTime.of(2023, Month.AUGUST, 21, 10, 0);
		final OffsetDateTime offsetDateTime = DateTimeUtils.toOffsetDateTimeFromUTC(dateTime);
		assertEquals(OffsetDateTime.of(dateTime, ZoneOffset.UTC), offsetDateTime);
	}

	@Test
	public void testToOffsetDateTime() {
		final LocalDateTime dateTime = LocalDateTime.of(2023, Month.AUGUST, 21, 10, 0);
		final OffsetDateTime offsetDateTime = DateTimeUtils.toOffsetDateTime(dateTime, ZoneOffset.ofHours(-5), ZoneOffset.ofHours(2));
		assertEquals(OffsetDateTime.of(2023, Month.AUGUST.getValue(), 21, 17, 0, 0, 0, ZoneOffset.ofHours(2)), offsetDateTime);
	}

	@Test
	public void testToLocalDateTimeUTC() {
		final OffsetDateTime offsetDateTime = OffsetDateTime.of(2023, Month.AUGUST.getValue(), 21, 15, 0, 0, 0, ZoneOffset.ofHours(5));
		final LocalDateTime localDateTime = DateTimeUtils.toLocalDateTimeUTC(offsetDateTime);
		assertEquals(LocalDateTime.of(2023, Month.AUGUST.getValue(), 21, 10, 0), localDateTime);
	}

	@Test
	public void testToLocalDateTime() {
		final OffsetDateTime offsetDateTime = OffsetDateTime.of(2023, Month.AUGUST.getValue(), 21, 15, 0, 0, 0, ZoneOffset.ofHours(5));
		final LocalDateTime localDateTime = DateTimeUtils.toLocalDateTime(offsetDateTime, ZoneOffset.ofHours(-5));
		assertEquals(LocalDateTime.of(2023, Month.AUGUST, 21, 5, 0), localDateTime);
	}

	@Test
	public void testToLastDayOfMonth() {
		final LocalDate date = LocalDate.of(2023, Month.FEBRUARY, 15);
		final LocalDate lastDay = DateTimeUtils.toLastDayOfMonth(date);
		assertEquals(LocalDate.of(2023, Month.FEBRUARY, 28), lastDay);
	}

	@Test
	public void testAge() {
		final LocalDate birthDay = LocalDate.of(2000, Month.FEBRUARY, 1);
		LocalDate when = LocalDate.of(2023, Month.MARCH, 1);
		int age = DateTimeUtils.age(birthDay, when);
		assertEquals(23, age);

		when = LocalDate.of(2023, Month.JANUARY, 1);
		age = DateTimeUtils.age(birthDay, when);
		assertEquals(22, age);
	}

	@Test
	public void testIsBirthday() {
		final LocalDate birthday = LocalDate.of(2000, Month.FEBRUARY, 29);
		LocalDate when = LocalDate.of(2023, Month.FEBRUARY, 28);
		assertTrue(DateTimeUtils.isBirthday(birthday, when));

		when = LocalDate.of(2024, Month.FEBRUARY, 29);
		assertTrue(DateTimeUtils.isBirthday(birthday, when));

		assertTrue(DateTimeUtils.isBirthday(LocalDate.of(2001, Month.FEBRUARY, 28), LocalDate.of(2020, Month.FEBRUARY, 28)));
	}
}