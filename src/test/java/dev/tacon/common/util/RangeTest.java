package dev.tacon.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

public class RangeTest {

	@Test
	public void testIsValid() {
		assertTrue(Range.isValid(Integer.valueOf(1), Integer.valueOf(5)));
		assertTrue(Range.isValid(null, Integer.valueOf(5)));
		assertTrue(Range.isValid(Integer.valueOf(5), null));
		assertFalse(Range.isValid(Integer.valueOf(6), Integer.valueOf(5)));
	}

	@Test
	public void testContains() {
		final Range<Integer> range = Range.between(Integer.valueOf(2), Integer.valueOf(5));
		assertTrue(range.contains(Integer.valueOf(3)));
		assertTrue(range.containsInclusive(Integer.valueOf(2)));
		assertTrue(range.containsInclusive(Integer.valueOf(5)));
		assertFalse(range.containsExclusive(Integer.valueOf(2)));
		assertFalse(range.containsExclusive(Integer.valueOf(5)));
	}

	@Test
	public void testIsBefore() {
		final Range<Integer> range = Range.between(Integer.valueOf(2), Integer.valueOf(5));
		assertTrue(range.isBefore(Integer.valueOf(6)));
		assertFalse(range.isBefore(Integer.valueOf(5)));
		assertFalse(range.isBefore(Integer.valueOf(2)));
		assertFalse(range.isBefore(Integer.valueOf(1)));
	}

	@Test
	public void testIsAfter() {
		final Range<Integer> range = Range.between(Integer.valueOf(2), Integer.valueOf(5));
		assertFalse(range.isAfter(Integer.valueOf(6)));
		assertFalse(range.isAfter(Integer.valueOf(5)));
		assertFalse(range.isAfter(Integer.valueOf(2)));
		assertTrue(range.isAfter(Integer.valueOf(1)));
	}

	@Test
	public void testIsOverlapping() {
		final Range<Integer> range1 = Range.between(Integer.valueOf(2), Integer.valueOf(5));
		final Range<Integer> range2 = Range.between(Integer.valueOf(4), Integer.valueOf(7));
		assertTrue(range1.isOverlapping(range2));

		final Range<Integer> range3 = Range.between(Integer.valueOf(6), Integer.valueOf(9));
		assertFalse(range1.isOverlapping(range3));
	}

	@Test
	public void testIntersect() {
		final Range<Integer> range1 = Range.between(Integer.valueOf(2), Integer.valueOf(5));
		final Range<Integer> range2 = Range.between(Integer.valueOf(4), Integer.valueOf(7));
		final Optional<Range<Integer>> intersect = range1.intersect(range2);
		assertTrue(intersect.isPresent());
		assertEquals(Range.between(Integer.valueOf(4), Integer.valueOf(5)), intersect.get());

		final Range<Integer> range3 = Range.between(Integer.valueOf(6), Integer.valueOf(9));
		final Optional<Range<Integer>> noIntersect = range1.intersect(range3);
		assertFalse(noIntersect.isPresent());
	}

	@Test
	public void testIsPoint() {
		final Range<Integer> point = Range.point(Integer.valueOf(5));
		assertTrue(point.isPoint());

		final Range<Integer> range = Range.between(Integer.valueOf(2), Integer.valueOf(5));
		assertFalse(range.isPoint());

		final Range<Integer> infinite = Range.infinite();
		assertFalse(infinite.isPoint());
	}

	@Test
	public void testIsBounded() {
		final Range<Integer> range = Range.between(Integer.valueOf(2), Integer.valueOf(5));
		assertTrue(range.isBounded());

		final Range<Integer> unbounded1 = Range.starting(Integer.valueOf(2));
		assertFalse(unbounded1.isBounded());

		final Range<Integer> unbounded2 = Range.ending(Integer.valueOf(5));
		assertFalse(unbounded2.isBounded());

		final Range<Integer> infinite = Range.infinite();
		assertFalse(infinite.isBounded());
	}

	@Test
	public void testIsInfinite() {
		final Range<Integer> range = Range.between(Integer.valueOf(2), Integer.valueOf(5));
		assertFalse(range.isInfinite());

		final Range<Integer> infinite = Range.infinite();
		assertTrue(infinite.isInfinite());
	}
}