package dev.tacon.common.util.function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class BooleanConsumerTest {

	@Test
	void testAccept() {
		final BooleanConsumer consumer = value -> assertTrue(value); // This consumer simply asserts that the value is true.
		consumer.accept(true);
	}

	@Test
	void testAndThen() {
		final StringBuilder result = new StringBuilder();

		final BooleanConsumer firstConsumer = value -> {
			if (value) {
				result.append("firstTrue");
			} else {
				result.append("firstFalse");
			}
		};

		final BooleanConsumer secondConsumer = value -> {
			if (value) {
				result.append("SecondTrue");
			} else {
				result.append("SecondFalse");
			}
		};

		final BooleanConsumer composedConsumer = firstConsumer.andThen(secondConsumer);

		// Test for true value
		composedConsumer.accept(true);
		assertEquals("firstTrueSecondTrue", result.toString());

		result.setLength(0); // Reset the result

		// Test for false value
		composedConsumer.accept(false);
		assertEquals("firstFalseSecondFalse", result.toString());
	}

	@Test
	void testAndThenWithNull() {
		final BooleanConsumer consumer = value -> {}; // A do-nothing consumer

		assertThrows(NullPointerException.class, () -> {
			consumer.andThen(null);
		});
	}
}
