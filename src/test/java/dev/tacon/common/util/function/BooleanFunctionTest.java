package dev.tacon.common.util.function;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class BooleanFunctionTest {

	@Test
	void testApplyForBooleanToInteger() {
		final BooleanFunction<Integer> function = value -> Integer.valueOf(value ? 1 : 0);

		assertEquals(Integer.valueOf(1), function.apply(true));
		assertEquals(Integer.valueOf(0), function.apply(false));
	}

	@Test
	void testApplyForBooleanToString() {
		final BooleanFunction<String> function = value -> value ? "True" : "False";

		assertEquals("True", function.apply(true));
		assertEquals("False", function.apply(false));
	}
}
