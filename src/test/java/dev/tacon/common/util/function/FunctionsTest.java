package dev.tacon.common.util.function;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

public class FunctionsTest {

	@Test
	public void testNullRunnable() {
		assertDoesNotThrow(() -> Functions.NULL_RUNNABLE.run());
	}

	@Test
	public void testNullSupplier() {
		assertNull(Functions.nullSupplier().get());
	}

	@Test
	public void testNullFunction() {
		assertNull(Functions.nullFunction().apply("input"));
	}

	@Test
	public void testNullBiFunction() {
		assertNull(Functions.nullBiFunction().apply("input1", "input2"));
	}

	@Test
	public void testNullIntFunction() {
		assertNull(Functions.nullIntFunction().apply(5));
	}

	@Test
	public void testNullConsumer() {
		assertDoesNotThrow(() -> Functions.nullConsumer().accept("input"));
	}

	@Test
	public void testNullBiConsumer() {
		assertDoesNotThrow(() -> Functions.nullBiConsumer().accept("input1", "input2"));
	}

	@Test
	public void testAcceptAll() {
		assertTrue(Functions.acceptAll().test("input"));
	}

	@Test
	public void testDeclineAll() {
		assertFalse(Functions.declineAll().test("input"));
	}

	@Test
	public void testBiAcceptAll() {
		assertTrue(Functions.biAcceptAll().test("input1", "input2"));
	}

	@Test
	public void testBiDeclineAll() {
		assertFalse(Functions.biDeclineAll().test("input1", "input2"));
	}

	@Test
	public void testAcceptFirst() {
		assertEquals("first", Functions.acceptFirst().apply("first", "second"));
	}

	@Test
	public void testAcceptSecond() {
		assertEquals("second", Functions.acceptSecond().apply("first", "second"));
	}

	@Test
	public void testAcceptFirstIf() {
		assertEquals("first", Functions.<String> acceptFirstIf(s -> s.startsWith("f")).apply("first", "second"));
		assertEquals("second", Functions.<String> acceptFirstIf(s -> s.startsWith("z")).apply("first", "second"));
	}

	@Test
	public void testAcceptSecondIf() {
		assertEquals("second", Functions.<String> acceptSecondIf(s -> s.startsWith("s")).apply("first", "second"));
		assertEquals("first", Functions.<String> acceptSecondIf(s -> s.startsWith("z")).apply("first", "second"));
	}

	@Test
	public void testConsumerAsFunction() {
		final List<String> list = new ArrayList<>();
		list.add("input1");
		Functions.<String> consumerAsFunction(list::add).apply("input2");
		assertEquals(2, list.size());
	}

	@Test
	public void testSupplierAsFunction() {
		assertEquals("fixed", Functions.supplierAsFunction(() -> "fixed").apply("input"));
	}

	@Test
	public void testRunIfTrue() {
		final boolean[] executed = { false };
		Functions.runIfTrue(() -> executed[0] = true).accept(true);
		assertTrue(executed[0]);
	}

	@Test
	public void testMergeGreatest() {
		assertEquals("b", Functions.<String> mergeGreatest().apply("a", "b"));
	}

	@Test
	public void testMergeLeast() {
		assertEquals("a", Functions.<String> mergeLeast().apply("a", "b"));
	}

	@Test
	public void testDistinctByKey() {
		final List<String> list = Arrays.asList("apple", "banana", "apple", "cherry");
		final List<String> distinct = list.stream()
				.filter(Functions.distinctByKey(Function.identity()))
				.collect(Collectors.toList());
		assertEquals(3, distinct.size());
	}

	// Add tests for other methods if necessary
}
