package dev.tacon.common.lang;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class StringUtilsTest {

	@Test
	public void testIsNullOrEmpty() {
		assertTrue(StringUtils.isNullOrEmpty(null));
		assertTrue(StringUtils.isNullOrEmpty(""));
		assertFalse(StringUtils.isNullOrEmpty("text"));
	}

	@Test
	public void testIsNotNullNorEmpty() {
		assertFalse(StringUtils.isNotNullNorEmpty(null));
		assertFalse(StringUtils.isNotNullNorEmpty(""));
		assertTrue(StringUtils.isNotNullNorEmpty("text"));
	}

	@Test
	public void testIsNullOrBlank() {
		assertTrue(StringUtils.isNullOrBlank(null));
		assertTrue(StringUtils.isNullOrBlank(" "));
		assertFalse(StringUtils.isNullOrBlank("text"));
	}

	@Test
	public void testIsNotNullNorBlank() {
		assertFalse(StringUtils.isNotNullNorBlank(null));
		assertFalse(StringUtils.isNotNullNorBlank(" "));
		assertTrue(StringUtils.isNotNullNorBlank("text"));
	}

	@Test
	public void testEmptyToNull() {
		assertNull(StringUtils.emptyToNull(""));
		assertEquals("text", StringUtils.emptyToNull("text"));
	}

	@Test
	public void testNullToEmpty() {
		assertEquals("", StringUtils.nullToEmpty(null));
		assertEquals("text", StringUtils.nullToEmpty("text"));
	}

	@Test
	public void testStripToNull() {
		assertNull(StringUtils.stripToNull(null));
		assertNull(StringUtils.stripToNull(" "));
		assertEquals("text", StringUtils.stripToNull(" text "));
	}

	@Test
	public void testStripToEmpty() {
		assertEquals("", StringUtils.stripToEmpty(null));
		assertEquals("", StringUtils.stripToEmpty(" "));
		assertEquals("text", StringUtils.stripToEmpty(" text "));
	}

	@Test
	public void testInsert() {
		assertEquals("teXst", StringUtils.insert("test", "X", 2));
		assertEquals("Xtest", StringUtils.insert("test", "X", 0));
		assertEquals("testX", StringUtils.insert("test", "X", 4));
	}

	@Test
	public void testRemove() {
		assertEquals("tst", StringUtils.remove("test", 1, 2));
		assertEquals("est", StringUtils.remove("test", 0, 1));
		assertEquals("tes", StringUtils.remove("test", 3, 4));
	}

	@Test
	public void testReplace() {
		assertEquals("tXst", StringUtils.replace("test", 1, 2, s -> "X"));
		assertEquals("Xest", StringUtils.replace("test", 0, 1, s -> "X"));
		assertEquals("tesX", StringUtils.replace("test", 3, 4, s -> "X"));
	}

	@Test
	public void testRemoveLeadingChars() {
		assertEquals("test", StringUtils.removeLeadingChars("ZZtest", 'Z'));
		assertEquals("test", StringUtils.removeLeadingChars("test", 'Z'));
	}

	@Test
	public void testRemoveTrailingChars() {
		assertEquals("test", StringUtils.removeTrailingChars("testZZ", 'Z'));
		assertEquals("test", StringUtils.removeTrailingChars("test", 'Z'));
	}

	@Test
	public void testCapitalize() {
		assertEquals("Test", StringUtils.capitalize("test"));
		assertEquals("Test String", StringUtils.capitalize("test string"));
	}

	@Test
	public void testGenerateChar() {
		assertEquals("XXXX", StringUtils.generate('X', 4));
		assertEquals("", StringUtils.generate('X', 0));
	}

	@Test
	public void testGenerateString() {
		assertEquals("testtest", StringUtils.generate("test", 2));
		assertEquals("", StringUtils.generate("test", 0));
	}

	@Test
	public void testLeft() {
		assertEquals("te", StringUtils.left("test", 2));
		assertEquals("test", StringUtils.left("test", 5));
	}

	@Test
	public void testRight() {
		assertEquals("st", StringUtils.right("test", 2));
		assertEquals("test", StringUtils.right("test", 5));
	}

	@Test
	public void testPadLeft() {
		assertEquals("XXtest", StringUtils.padLeft("test", 6, 'X'));
		assertEquals("tes", StringUtils.padLeft("test", 3, 'X'));
	}

	@Test
	public void testPadRight() {
		assertEquals("testXX", StringUtils.padRight("test", 6, 'X'));
		assertEquals("tes", StringUtils.padRight("test", 3, 'X'));
	}

	@Test
	public void testContainsIgnoreCase() {
		assertTrue(StringUtils.containsIgnoreCase("test", "TE"));
		assertFalse(StringUtils.containsIgnoreCase("test", "TX"));
	}

	@Test
	public void testFindCommonPrefix() {
		assertEquals("te", StringUtils.findCommonPrefix("test", "teach"));
		assertNull(StringUtils.findCommonPrefix("test", "best"));
	}

	@Test
	public void testEncodeHTML() {
		assertEquals("&amp;", StringUtils.encodeHTML("&"));
		assertEquals("&lt;", StringUtils.encodeHTML("<"));
	}

	@Test
	public void testEncodeASCII() {
		assertEquals("Ae", StringUtils.encodeASCII("Àé"));
	}
}