package dev.tacon.common.lang;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class BooleanUtilsTest {

	@Test
	void testNot() {
		assertNull(BooleanUtils.not(null));
		assertEquals(Boolean.FALSE, BooleanUtils.not(Boolean.TRUE));
		assertEquals(Boolean.TRUE, BooleanUtils.not(Boolean.FALSE));
	}

	@Test
	void testIsTrue() {
		assertFalse(BooleanUtils.isTrue(null));
		assertTrue(BooleanUtils.isTrue(Boolean.TRUE));
		assertFalse(BooleanUtils.isTrue(Boolean.FALSE));
	}

	@Test
	void testIsFalse() {
		assertFalse(BooleanUtils.isFalse(null));
		assertFalse(BooleanUtils.isFalse(Boolean.TRUE));
		assertTrue(BooleanUtils.isFalse(Boolean.FALSE));
	}

	@Test
	void testAnd() {
		assertTrue(BooleanUtils.and(Boolean.TRUE, Boolean.TRUE));
		assertFalse(BooleanUtils.and(Boolean.TRUE, Boolean.FALSE));
		assertFalse(BooleanUtils.and(Boolean.FALSE, Boolean.TRUE));
		assertFalse(BooleanUtils.and(Boolean.FALSE, Boolean.FALSE));
		assertFalse(BooleanUtils.and(Boolean.TRUE, null));
		assertFalse(BooleanUtils.and(null, Boolean.TRUE));
		assertFalse(BooleanUtils.and(null, null));
	}

	@Test
	void testOr() {
		assertTrue(BooleanUtils.or(Boolean.TRUE, Boolean.TRUE));
		assertTrue(BooleanUtils.or(Boolean.TRUE, Boolean.FALSE));
		assertTrue(BooleanUtils.or(Boolean.FALSE, Boolean.TRUE));
		assertFalse(BooleanUtils.or(Boolean.FALSE, Boolean.FALSE));
		assertTrue(BooleanUtils.or(Boolean.TRUE, null));
		assertTrue(BooleanUtils.or(null, Boolean.TRUE));
		assertFalse(BooleanUtils.or(null, null));
	}
}