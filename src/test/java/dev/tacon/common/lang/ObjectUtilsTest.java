package dev.tacon.common.lang;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

public class ObjectUtilsTest {

	// Sample beans to use for testing
	public static class TestBean {

		private String name;
		private int age;
		private BigDecimal salary;

		public String getName() {
			return this.name;
		}

		public void setName(final String name) {
			this.name = name;
		}

		public int getAge() {
			return this.age;
		}

		public void setAge(final int age) {
			this.age = age;
		}

		public BigDecimal getSalary() {
			return this.salary;
		}

		public void setSalary(final BigDecimal salary) {
			this.salary = salary;
		}
	}

	@Test
	void testCopyBeanValuesSameType() {
		final TestBean source = new TestBean();
		source.setName("John");
		source.setAge(25);
		source.setSalary(new BigDecimal("1000.00"));

		final TestBean target = new TestBean();

		ObjectUtils.copyBeanValues(source, target);

		assertEquals("John", target.getName());
		assertEquals(25, target.getAge());
		assertEquals(new BigDecimal("1000.00"), target.getSalary());
	}

	@Test
	void testCopyBeanValuesWithBeanClass() {
		final TestBean source = new TestBean();
		source.setName("Jane");
		source.setAge(30);
		source.setSalary(new BigDecimal("1000"));

		final TestBean target = new TestBean();

		ObjectUtils.copyBeanValues(TestBean.class, source, target);

		assertEquals("Jane", target.getName());
		assertEquals(30, target.getAge());
		assertEquals(new BigDecimal("1000"), target.getSalary());
	}

	@Test
	void testCompareOrEqual() {
		assertTrue(ObjectUtils.compareOrEqual("test", "test"));
		assertFalse(ObjectUtils.compareOrEqual("test1", "test2"));

		assertTrue(ObjectUtils.compareOrEqual(null, null));
		assertFalse(ObjectUtils.compareOrEqual(null, "test2"));

		assertTrue(ObjectUtils.compareOrEqual(Integer.valueOf(5), Integer.valueOf(5)));
		assertFalse(ObjectUtils.compareOrEqual(Integer.valueOf(5), Integer.valueOf(6)));

		assertTrue(ObjectUtils.compareOrEqual(BigDecimal.ONE, BigDecimal.ONE));
		assertTrue(ObjectUtils.compareOrEqual(new BigDecimal("1"), new BigDecimal("1.00")));
		assertFalse(ObjectUtils.compareOrEqual(new BigDecimal("1"), new BigDecimal("0")));
	}

	@Test
	void testCompareOrEqualBeans() {
		final TestBean bean1 = new TestBean();
		bean1.setName("John");
		bean1.setAge(25);
		bean1.setSalary(new BigDecimal("1000"));

		final TestBean bean2 = new TestBean();
		bean2.setName("John");
		bean2.setAge(25);
		bean2.setSalary(new BigDecimal("1000.00"));

		final TestBean bean3 = new TestBean();
		bean3.setName("Jane");
		bean3.setAge(30);
		bean3.setSalary(new BigDecimal("1000"));

		assertTrue(ObjectUtils.compareOrEqualBeans(bean1, bean2));
		assertFalse(ObjectUtils.compareOrEqualBeans(bean1, bean3));
	}
}
