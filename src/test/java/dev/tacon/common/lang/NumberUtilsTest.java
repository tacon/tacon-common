package dev.tacon.common.lang;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class NumberUtilsTest {

	@BeforeEach
	public void setup() {
		// Potenzialmente qualsiasi configurazione iniziale necessaria
	}

	@Test
	public void testToByte() {
		Assertions.assertNull(NumberUtils.toByte(null));
		Assertions.assertEquals(Byte.valueOf((byte) 5), NumberUtils.toByte(Double.valueOf(5.45)));
		Assertions.assertEquals(Byte.valueOf((byte) 10), NumberUtils.toByte(Double.valueOf(10), (byte) 5));
	}

	@Test
	public void testToShort() {
		Assertions.assertNull(NumberUtils.toShort(null));
		Assertions.assertEquals(Short.valueOf((short) 5), NumberUtils.toShort(Double.valueOf(5.45)));
		Assertions.assertEquals(Short.valueOf((short) 10), NumberUtils.toShort(Double.valueOf(10), (short) 5));
	}

	// ... (Ripetere per toInteger, toLong, etc.)

	@Test
	public void testToBigDecimal() {
		Assertions.assertNull(NumberUtils.toBigDecimal((Number) null));
		Assertions.assertTrue(BigDecimal.valueOf(5.45).compareTo(NumberUtils.toBigDecimal(Double.valueOf(5.45))) == 0);
		Assertions.assertTrue(BigDecimal.valueOf(10).compareTo(NumberUtils.toBigDecimal(Double.valueOf(10), BigDecimal.ZERO)) == 0);
	}

	// ... (Ripetere per toBigInteger, altri overloads di toBigDecimal, etc.)

	@Test
	public void testParseInteger() {
		Assertions.assertNull(NumberUtils.parseInteger(null));
		Assertions.assertEquals(Integer.valueOf(5), NumberUtils.parseInteger("5"));
		Assertions.assertEquals(Integer.valueOf(5), NumberUtils.parseInteger("5", 10));
		Assertions.assertEquals(Integer.valueOf(10), NumberUtils.parseInteger(null, 10));
	}

	// ... (Ripetere per parseShort, parseLong, parseDouble, etc.)

	@Test
	public void testZeroToNull() {
		Assertions.assertNull(NumberUtils.zeroToNull((Integer) null));
		Assertions.assertNull(NumberUtils.zeroToNull(NumberUtils.INTEGER_ZERO));
		Assertions.assertEquals(Integer.valueOf(5), NumberUtils.zeroToNull(Integer.valueOf(5)));
	}

	@Test
	public void testIsPositiveOrZero() {
		Assertions.assertFalse(NumberUtils.isPositiveOrZero(null));
		Assertions.assertTrue(NumberUtils.isPositiveOrZero(Double.valueOf(5.45)));
		Assertions.assertTrue(NumberUtils.isPositiveOrZero(Double.valueOf(0)));
		Assertions.assertFalse(NumberUtils.isPositiveOrZero(Double.valueOf(-5.45)));
	}

	// ... (Ripetere per isPositive, isNegative)

	@Test
	public void testRound() {
		Assertions.assertEquals(5.5, NumberUtils.round(5.45, 1));
		Assertions.assertEquals(5.45, NumberUtils.round(5.453, 2, RoundingMode.HALF_UP));
	}

	// ...

	@Test
	public void testFormat() {
		Assertions.assertEquals("5.45", NumberUtils.format(5.45, 2).replace(',', '.'));
		Assertions.assertEquals("5", NumberUtils.format(5, NumberUtils.NumberPattern.INT));
		// ... (Aggiungere altri casi di test)
	}

	@Test
	public void testParseBigDecimal() {
		Assertions.assertNull(NumberUtils.parseBigDecimal(""));
		Assertions.assertNull(NumberUtils.parseBigDecimal("abc"));
		final BigDecimal v = new BigDecimal("5.45");

		Assertions.assertEquals(BigDecimal.valueOf(5.45), NumberUtils.parseBigDecimal(NumberUtils.format(v, 2)));
	}
}
