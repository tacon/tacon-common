package dev.tacon.common.lang.reflect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;

public class ReflectionUtilsTest {

	@SuppressWarnings("unused")
	private static class TestBean {

		private String property;

		public String getProperty() {
			return this.property;
		}

		public void setProperty(final String property) {
			this.property = property;
		}

		public boolean isFlag() {
			return true;
		}
	}

	@Test
	public void testFindGetterMethod() {
		final Class<?> clazz = TestBean.class;
		final String propertyName = "property";
		final Method getterMethod = ReflectionUtils.findGetterMethod(clazz, propertyName);

		assertNotNull(getterMethod, "Expected a valid getter method");
		assertEquals("getProperty", getterMethod.getName(), "Expected method name to match");
	}

	@Test
	public void testFindSetterMethod() {
		final Class<?> clazz = TestBean.class;
		final String propertyName = "property";
		final Method setterMethod = ReflectionUtils.findSetterMethod(clazz, propertyName, String.class);

		assertNotNull(setterMethod, "Expected a valid setter method");
		assertEquals("setProperty", setterMethod.getName(), "Expected method name to match");
	}

	@Test
	public void testFindMethodForGetter() {
		final Class<?> clazz = TestBean.class;
		final String propertyName = "property";
		final Method method = ReflectionUtils.findMethod(clazz, propertyName, new String[] { "get", "is" });

		assertNotNull(method, "Expected a valid method");
		assertEquals("getProperty", method.getName(), "Expected method name to match");
	}

	@Test
	public void testFindMethodForIs() {
		final Class<?> clazz = TestBean.class;
		final String propertyName = "flag";
		final Method method = ReflectionUtils.findMethod(clazz, propertyName, new String[] { "get", "is" });

		assertNotNull(method, "Expected a valid method");
		assertEquals("isFlag", method.getName(), "Expected method name to match");
	}

	@Test
	public void testFindMethodNoPrefix() {
		final Class<?> clazz = TestBean.class;
		final String methodName = "isFlag";
		final Method method = ReflectionUtils.findMethod(clazz, methodName, new String[0]);

		assertNotNull(method, "Expected a valid method");
		assertEquals("isFlag", method.getName(), "Expected method name to match");
	}

	@Test
	public void testFindMethodNonExisting() {
		final Class<?> clazz = TestBean.class;
		final String propertyName = "nonExisting";
		final Method method = ReflectionUtils.findMethod(clazz, propertyName, new String[] { "get", "is" });

		assertNull(method, "Expected no method to be found");
	}
}