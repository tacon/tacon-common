package dev.tacon.common.lang;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class ClassUtilsTest {

	@Test
	void testIsPrimitiveNumber() {
		assertTrue(ClassUtils.isPrimitiveNumber(int.class));
		assertTrue(ClassUtils.isPrimitiveNumber(double.class));
		assertFalse(ClassUtils.isPrimitiveNumber(char.class));
		assertFalse(ClassUtils.isPrimitiveNumber(boolean.class));
	}

	@Test
	void testGetPrimitive() {
		assertEquals(int.class, ClassUtils.getPrimitive(Integer.class));
		assertEquals(double.class, ClassUtils.getPrimitive(Double.class));
		assertNull(ClassUtils.getPrimitive(String.class)); // Assuming PRIMITIVES map doesn't contain non-primitive mappings.
	}

	@Test
	void testGetWrapper() {
		assertEquals(Integer.class, ClassUtils.getWrapper(int.class));
		assertEquals(Double.class, ClassUtils.getWrapper(double.class));
		assertNull(ClassUtils.getWrapper(String.class)); // Assuming WRAPPERS map doesn't contain non-wrapper mappings.
	}

	@Test
	void testGetSuperclasses() {
		final List<Class<?>> superclasses = ClassUtils.getSuperclasses(IOException.class);
		assertFalse(superclasses.contains(IOException.class));
		assertTrue(superclasses.contains(Exception.class));
		assertTrue(superclasses.contains(Throwable.class));
	}

	@Test
	void testGetInterfaces() {
		final Set<Class<?>> interfaces = ClassUtils.getInterfaces(LinkedList.class);
		assertFalse(interfaces.contains(LinkedList.class));
		assertTrue(interfaces.contains(List.class));
		assertTrue(interfaces.contains(Deque.class));
		assertTrue(interfaces.contains(Queue.class));
		assertTrue(interfaces.contains(Collection.class));
	}
}