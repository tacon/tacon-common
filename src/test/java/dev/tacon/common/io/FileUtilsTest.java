package dev.tacon.common.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;

public class FileUtilsTest {

	@Test
	public void testGetExtensionFromPath() {
		assertEquals("txt", FileUtils.getExtension(Path.of("/tmp/example.txt")));
		assertEquals("", FileUtils.getExtension(Path.of("/tmp/example")));
		assertEquals("", FileUtils.getExtension(Path.of("/tmp/example.")));
	}

	@Test
	public void testGetExtensionFromString() {
		assertEquals("txt", FileUtils.getExtension("/tmp/example.txt"));
		assertEquals("txt", FileUtils.getExtension("/tmp/example.xml.txt"));
		assertEquals("", FileUtils.getExtension("/tmp/example"));
		assertEquals("", FileUtils.getExtension("/tmp/example."));
	}

	@Test
	public void testRemoveExtension() {
		assertEquals("/tmp/example", FileUtils.removeExtension("/tmp/example.txt"));
		assertEquals("/tmp/example", FileUtils.removeExtension("/tmp/example"));
		assertEquals("/tmp/example.", FileUtils.removeExtension("/tmp/example."));

		assertEquals("/tmp/example.txt/hello", FileUtils.removeExtension("/tmp/example.txt/hello"));
	}

	@Test
	public void testGetSystemTempDir() {
		assertEquals(System.getProperty("java.io.tmpdir"), FileUtils.getSystemTempDir().toString());
	}

	@Test
	public void testGetSystemUserDir() {
		assertEquals(System.getProperty("user.dir"), FileUtils.getSystemUserDir().toString());
	}

	@Test
	public void testGetSystemUserHome() {
		assertEquals(System.getProperty("user.home"), FileUtils.getSystemUserHome().toString());
	}

	@Test
	public void testRecursiveDelete() throws IOException {

		final Path dirToCreate = Files.createTempDirectory("testRecursiveDelete").resolve("dirToCreate");
		Files.createDirectory(dirToCreate);

		final Path fileToCreate = dirToCreate.resolve("fileToCreate.txt");
		Files.createFile(fileToCreate);

		final Path subDir = Files.createDirectory(dirToCreate.resolve("subDir"));
		Files.createFile(subDir.resolve("files2.txt"));

		FileUtils.recursiveDelete(dirToCreate);

		assertFalse(Files.exists(dirToCreate));
	}
}
