package dev.tacon.common.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

import org.junit.jupiter.api.Test;

public class IOUtilsTest {

	private static final String TEST_STRING = "Hello, world!";

	@Test
	public void testToBytes() throws IOException {
		final InputStream is = new ByteArrayInputStream(TEST_STRING.getBytes());
		final byte[] result = IOUtils.toBytes(is);
		assertEquals(TEST_STRING, new String(result));
	}

	@Test
	public void testToStringFromReader() throws IOException {
		final Reader reader = new StringReader(TEST_STRING);
		final String result = IOUtils.toString(reader);
		assertEquals(TEST_STRING, result);
	}

	@Test
	public void testToStringFromInputStream() throws IOException {
		final InputStream is = new ByteArrayInputStream(TEST_STRING.getBytes());
		final String result = IOUtils.toString(is);
		assertEquals(TEST_STRING, result);
	}

	@Test
	public void testCopy() throws IOException {
		final InputStream is = new ByteArrayInputStream(TEST_STRING.getBytes());
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		final long bytesRead = IOUtils.copy(is, os);
		assertEquals(TEST_STRING.length(), bytesRead);
		assertEquals(TEST_STRING, os.toString());
	}
}