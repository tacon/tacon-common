package dev.tacon.common.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class CommonMediaTypeTest {

	@Test
	public void testUniqueMimeTypeAndExtension() {
		final Set<String> mimeTypes = new HashSet<>();
		final Set<String> extensions = new HashSet<>();
		for (final CommonMediaType mediaType : CommonMediaType.values()) {
			for (final String mimeType : mediaType.getMimeTypes()) {
				if (!mimeTypes.add(mimeType)) {
					fail("Duplicate MIME type " + mimeType);
				}
			}
			for (final String extension : mediaType.getExtensions()) {
				if (!extensions.add(extension)) {
					fail("Duplicate extension " + extension);
				}
			}
		}
	}

	@Test
	public void testOfExtension() {
		final Set<CommonMediaType> jpgTypes = CommonMediaType.ofExtension("jpg");
		assertTrue(jpgTypes.contains(CommonMediaType.IMAGE_JPEG));

		final Set<CommonMediaType> txtTypes = CommonMediaType.ofExtension(".txt");
		assertTrue(txtTypes.contains(CommonMediaType.TEXT_PLAIN));

		final Set<CommonMediaType> invalidTypes = CommonMediaType.ofExtension("___");
		assertTrue(invalidTypes.isEmpty());

		final Set<CommonMediaType> emptyTypes = CommonMediaType.ofExtension("");
		assertTrue(emptyTypes.isEmpty());
	}

	@Test
	public void testOfMimeType() {
		final Set<CommonMediaType> plainTextTypes = CommonMediaType.ofMimeType("text/plain");
		assertTrue(plainTextTypes.contains(CommonMediaType.TEXT_PLAIN));

		final Set<CommonMediaType> jsonTypes = CommonMediaType.ofMimeType("application/json");
		assertTrue(jsonTypes.contains(CommonMediaType.APPLICATION_JSON));

		final Set<CommonMediaType> invalidTypes = CommonMediaType.ofMimeType("asd");
		assertTrue(invalidTypes.isEmpty());

		final Set<CommonMediaType> emptyTypes = CommonMediaType.ofMimeType("");
		assertTrue(emptyTypes.isEmpty());
	}

	@Test
	void testMimeTypeOf() {
		// Test case: known extension
		assertEquals("text/plain", CommonMediaType.mimeTypeOf("txt").orElse(null));

		// Test case: unknown extension
		assertTrue(CommonMediaType.mimeTypeOf("unknown").isEmpty());

		// Test case: extension with leading dot
		assertEquals("text/plain", CommonMediaType.mimeTypeOf(".txt").orElse(null));

		// Test case: null extension
		assertTrue(CommonMediaType.mimeTypeOf(null).isEmpty());

		// Test case: empty extension
		assertTrue(CommonMediaType.mimeTypeOf("").isEmpty());
	}

	@Test
	void testExtensionOf() {
		// Test case: known MIME type
		assertEquals("txt", CommonMediaType.extensionOf("text/plain").orElse(null));

		// Test case: unknown MIME type
		assertTrue(CommonMediaType.extensionOf("unknown/type").isEmpty());

		// Test case: MIME type with varying cases
		assertEquals("txt", CommonMediaType.extensionOf("TEXT/PLAIN").orElse(null));

		// Test case: null MIME type
		assertTrue(CommonMediaType.extensionOf(null).isEmpty());

		// Test case: empty MIME type
		assertTrue(CommonMediaType.extensionOf("").isEmpty());
	}

	@Test
	public void testGetMimeTypes() {
		assertEquals("text/plain", CommonMediaType.TEXT_PLAIN.getDefaultMimeType());
		assertEquals("image/jpeg", CommonMediaType.IMAGE_JPEG.getDefaultMimeType());
	}

	@Test
	public void testGetExtensions() {
		assertEquals("txt", CommonMediaType.TEXT_PLAIN.getDefaultExtension());
		assertEquals("jpg", CommonMediaType.IMAGE_JPEG.getDefaultExtension());
	}

	@Test
	public void testGetType() {
		assertEquals("text", CommonMediaType.TEXT_PLAIN.getType());
		assertEquals("image", CommonMediaType.IMAGE_JPEG.getType());
	}
}
