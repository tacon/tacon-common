package dev.tacon.common.io;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import dev.tacon.annotations.NonNullByDefault;
import dev.tacon.annotations.Nullable;

/**
 * Utility class for file and directory-related tasks.
 */
@NonNullByDefault
public final class FileUtils {

	/**
	 * Retrieves the file extension from a given Path.
	 *
	 * @param path Path object representing the file.
	 * @return File extension as a String or an empty String if there's no extension.
	 */
	public static String getExtension(final Path path) {
		return getExtension(path.getFileName().toString());
	}

	/**
	 * Retrieves the file extension from a given file name.
	 *
	 * @param fileName The name of the file.
	 * @return File extension as a String or an empty String if there's no extension.
	 */
	public static String getExtension(final String fileName) {
		final int extensionPos = fileName.lastIndexOf('.');
		if (extensionPos < 0 || extensionPos == fileName.length() - 1) {
			return "";
		}
		final int lastSeparator = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
		if (lastSeparator > extensionPos) {
			return "";
		}
		return fileName.substring(extensionPos + 1);

	}

	/**
	 * Removes the file extension from a given file name.
	 *
	 * @param fileName The name of the file.
	 * @return File name without the extension.
	 */
	public static String removeExtension(final String fileName) {
		final int extensionPos = fileName.lastIndexOf('.');
		if (extensionPos < 0 || extensionPos == fileName.length() - 1) {
			return fileName;
		}
		final int lastSeparator = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
		if (lastSeparator > extensionPos) {
			return fileName;
		}
		return fileName.substring(0, extensionPos);
	}

	/**
	 * Returns the temporary directory specified by the system property {@code java.io.tmpdir}.
	 *
	 * @return Path object representing the temporary directory.
	 */
	public static Path getSystemTempDir() {
		return Path.of(System.getProperty("java.io.tmpdir"));
	}

	/**
	 * Returns the user directory specified by the system property {@code user.dir}.
	 *
	 * @return Path object representing the user directory.
	 */
	public static Path getSystemUserDir() {
		return Path.of(System.getProperty("user.dir"));
	}

	/**
	 * Returns the user home directory specified by the system property {@code user.home}.
	 *
	 * @return Path object representing the user home directory.
	 */
	public static Path getSystemUserHome() {
		return Path.of(System.getProperty("user.home"));
	}

	/**
	 * Recursively deletes files and directories starting from the specified path.
	 *
	 * @param path starting path for recursive deletion
	 * @throws IOException if an I/O error occurs
	 */
	public static void recursiveDelete(final Path path) throws IOException {
		if (!Files.exists(requireNonNull(path, "path required"))) {
			return;
		}
		Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(final @Nullable Path file, final @Nullable BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(final @Nullable Path dir, final @Nullable IOException ex) throws IOException {
				if (ex == null) {
					Files.delete(dir);
					return FileVisitResult.CONTINUE;
				}
				throw ex;
			}
		});
	}

	private FileUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
