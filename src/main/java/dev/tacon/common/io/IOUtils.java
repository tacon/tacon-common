package dev.tacon.common.io;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;

import dev.tacon.annotations.NonNullByDefault;

/**
 * Utility class for various I/O operations.
 */
@NonNullByDefault
public final class IOUtils {

	private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

	/**
	 * Copies data from a InputStream to a byte array using the specified buffer size.
	 *
	 * @param is InputStream to convert
	 * @param bufferSize the size of the buffer to use
	 * @return byte array containing the data from the InputStream
	 * @throws IOException if an I/O error occurs
	 */
	public static byte[] toBytes(final InputStream is, final int bufferSize) throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			copy(is, baos, bufferSize);
			return baos.toByteArray();
		}
	}

	/**
	 * Copies data from a InputStream to a byte array.
	 *
	 * @param is InputStream to convert
	 * @return byte array containing the data from the InputStream
	 * @throws IOException if an I/O error occurs
	 */
	public static byte[] toBytes(final InputStream is) throws IOException {
		return toBytes(is, DEFAULT_BUFFER_SIZE);
	}

	/**
	 * Reads data from a Reader using the specified buffer size and returns it as a String.
	 *
	 * @param reader reader to convert
	 * @param bufferSize the size of the buffer to use
	 * @return String containing the data from the Reader
	 * @throws IOException if an I/O error occurs
	 */
	public static String toString(final Reader reader, final int bufferSize) throws IOException {
		try (final BufferedReader bufferedReader = new BufferedReader(reader)) {
			return toString(bufferedReader, bufferSize);
		}
	}

	/**
	 * Reads data from a Reader and returns it as a String.
	 *
	 * @param reader reader to convert
	 * @return string containing the data from the Reader
	 * @throws IOException if an I/O error occurs
	 */
	public static String toString(final Reader reader) throws IOException {
		return toString(reader, DEFAULT_BUFFER_SIZE);
	}

	/**
	 * Reads data from a BufferedReader using the specified buffer size and returns it as a String.
	 *
	 * @param bufferedReader bufferedReader to convert
	 * @param bufferSize the size of the buffer to use
	 * @return string containing the data from the BufferedReader
	 * @throws IOException if an I/O error occurs
	 */
	public static String toString(final BufferedReader bufferedReader, final int bufferSize) throws IOException {
		final StringBuilder sb = new StringBuilder();

		final char[] buf = new char[bufferSize];
		int i = 0;
		while ((i = bufferedReader.read(buf)) > 0) {
			sb.append(new String(buf, 0, i));
		}
		return sb.toString();
	}

	/**
	 * Reads data from a BufferedReader and returns it as a String.
	 *
	 * @param bufferedReader bufferedReader to convert
	 * @return String containing the data from the BufferedReader
	 * @throws IOException if an I/O error occurs
	 */
	public static String toString(final BufferedReader bufferedReader) throws IOException {
		return toString(bufferedReader, DEFAULT_BUFFER_SIZE);
	}

	/**
	 * Reads data from an InputStream using the specified buffer size and returns it as a String.
	 *
	 * @param is InputStream to convert
	 * @param bufferSize the size of the buffer to use
	 * @return string containing the data from the InputStream
	 * @throws IOException if an I/O error occurs
	 */
	public static String toString(final InputStream is, final int bufferSize) throws IOException {
		final StringWriter sw = new StringWriter();
		final char[] buffer = new char[bufferSize];
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
			int i;
			while ((i = reader.read(buffer)) != -1) {
				sw.write(buffer, 0, i);
			}
		}
		return sw.toString();
	}

	/**
	 * Reads data from an InputStream and returns it as a String.
	 *
	 * @param is InputStream to convert
	 * @return string containing the data from the InputStream
	 * @throws IOException if an I/O error occurs
	 */
	public static String toString(final InputStream is) throws IOException {
		return toString(is, DEFAULT_BUFFER_SIZE);
	}

	/**
	 * Copies data from an InputStream to an OutputStream using the specified buffer size.
	 *
	 * @param is InputStream to read from
	 * @param os OutputStream to write to
	 * @param bufferSize the size of the buffer to use
	 * @return the total number of bytes read from the InputStream
	 * @throws IOException tf an I/O error occurs
	 */
	public static long copy(final InputStream is, final OutputStream os, final int bufferSize) throws IOException {
		long bytesRead = 0L;
		final byte[] buf = new byte[bufferSize];
		int n;
		while ((n = is.read(buf)) > 0) {
			os.write(buf, 0, n);
			bytesRead += n;
		}
		return bytesRead;
	}

	/**
	 * Copies data from an InputStream to an OutputStream.
	 *
	 * @param is InputStream to read from
	 * @param os OutputStream to write to
	 * @return the total number of bytes read from the InputStream
	 * @throws IOException if an I/O error occurs
	 */
	public static long copy(final InputStream is, final OutputStream os) throws IOException {
		return copy(is, os, DEFAULT_BUFFER_SIZE);
	}

	private IOUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
