package dev.tacon.common.io;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import dev.tacon.annotations.NonNullByDefault;
import dev.tacon.annotations.Nullable;

/**
 * Enum representing common media types, including their MIME types and file extensions.
 *
 * <p>This enum facilitates the mapping between file extensions and their corresponding
 * MIME types, making it easier to handle content types in various contexts.</p>
 *
 * <p>Example usage for finding media types by extension:</p>
 *
 * <pre>
 * {@code
 *
 * Set<CommonMediaType> types = CommonMediaType.ofExtension("jpg");
 * }
 * </pre>
 *
 * <p>Example usage for finding media types by MIME type:</p>
 *
 * <pre>
 * {@code
 *
 * Set<CommonMediaType> types = CommonMediaType.ofMimeType("image/jpeg");
 * }
 * </pre>
 */
@NonNullByDefault
public enum CommonMediaType {

	TEXT_PLAIN("text/plain", "txt"),
	APPLICATION_OCTET_STREAM("application/octet-stream", "bin"),

	APPLICATION_7Z("application/x-7z-compressed", "7z"),
	APPLICATION_ABIWORD("application/x-abiword", "abw"),
	APPLICATION_BZIP("application/x-bzip", "bz"),
	APPLICATION_BZIP2("application/x-bzip2", "bz2"),
	APPLICATION_CDF("application/x-cdf", "cda"),
	APPLICATION_CSH("application/x-csh", "csh"),
	APPLICATION_EPUB_ZIP("application/epub+zip", "epub"),
	APPLICATION_FREEARC("application/x-freearc", "arc"),
	APPLICATION_GZIP("application/gzip", "gz"),
	APPLICATION_JAVA_ARCHIVE("application/java-archive", "jar"),
	APPLICATION_JSON("application/json", "json"),
	APPLICATION_JSONLD("application/ld+json", "jsonld"),
	APPLICATION_MSWORD("application/msword", "doc"),
	APPLICATION_OGG("application/ogg", "ogx"),
	APPLICATION_PDF("application/pdf", "pdf"),
	APPLICATION_PHP("application/x-httpd-php", "php"),
	APPLICATION_RTF("application/rtf", "rtf"),
	APPLICATION_SH("application/x-sh", "sh"),
	APPLICATION_TAR("application/x-tar", "tar"),
	APPLICATION_VND_AMAZON_EBOOK("application/vnd.amazon.ebook", "azw"),
	APPLICATION_VND_APPLE_INSTALLER_XML("application/vnd.apple.installer+xml", "mpkg"),
	APPLICATION_VND_MS_EXCEL("application/vnd.ms-excel", "xls"),
	APPLICATION_VND_MS_FONTOBJECT("application/vnd.ms-fontobject", "eot"),
	APPLICATION_VND_MS_POWERPOINT("application/vnd.ms-powerpoint", "ppt"),
	APPLICATION_VND_OASIS_PRESENTATION("application/vnd.oasis.opendocument.presentation", "odp"),
	APPLICATION_VND_OASIS_SPREADSHEET("application/vnd.oasis.opendocument.spreadsheet", "ods"),
	APPLICATION_VND_OASIS_TEXT("application/vnd.oasis.opendocument.text", "odt"),
	APPLICATION_VND_OPENXML_DOCUMENT("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx"),
	APPLICATION_VND_OPENXML_PRESENTATION("application/vnd.openxmlformats-officedocument.presentationml.presentation", "pptx"),
	APPLICATION_VND_OPENXML_SPREADSHEET("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx"),
	APPLICATION_VND_RAR("application/vnd.rar", "rar"),
	APPLICATION_VND_VISIO("application/vnd.visio", "vsd"),
	APPLICATION_XHTML_XML("application/xhtml+xml", "xhtml"),
	APPLICATION_XML("application/xml text/xml", "xml"),
	APPLICATION_ZIP("application/zip", "zip"),

	AUDIO_AAC("audio/aac", "aac"),
	AUDIO_MIDI("audio/midi audio/x-midi", "mid midi"),
	AUDIO_MPEG("audio/mpeg", "mp3"),
	AUDIO_OGG("audio/ogg", "oga"),
	AUDIO_OPUS("audio/opus", "opus"),
	AUDIO_WAV("audio/wav", "wav"),
	AUDIO_WEBM("audio/webm", "weba"),

	FONT_OTF("font/otf", "otf"),
	FONT_TTF("font/ttf", "ttf"),
	FONT_WOFF("font/woff", "woff"),
	FONT_WOFF2("font/woff2", "woff2"),

	IMAGE_AVIF("image/avif", "avif"),
	IMAGE_BMP("image/bmp", "bmp"),
	IMAGE_GIF("image/gif", "gif"),
	IMAGE_JPEG("image/jpeg", "jpg jpeg"),
	IMAGE_PNG("image/png", "png"),
	IMAGE_SVG_XML("image/svg+xml", "svg"),
	IMAGE_TIFF("image/tiff", "tif tiff"),
	IMAGE_VND_MS_ICON("image/vnd.microsoft.icon", "ico"),
	IMAGE_WEBP("image/webp", "webp"),

	TEXT_CALENDAR("text/calendar", "ics"),
	TEXT_CSS("text/css", "css"),
	TEXT_CSV("text/csv", "csv"),
	TEXT_HTML("text/html", "html htm"),
	TEXT_JAVASCRIPT("text/javascript", "js mjs"),

	VIDEO_3GPP("video/3gpp audio/3gpp", "3gp"),
	VIDEO_3GPP2("video/3gpp2 audio/3gpp2", "3g2"),
	VIDEO_MP2T("video/mp2t", "ts"),
	VIDEO_MP4("video/mp4", "mp4"),
	VIDEO_MPEG("video/mpeg", "mpeg"),
	VIDEO_OGG("video/ogg", "ogv"),
	VIDEO_WEBM("video/webm", "webm"),
	VIDEO_MSVIDEO("video/x-msvideo", "avi"),
	;

	/**
	 * Represents the default media type for text-based content.
	 */
	public static final CommonMediaType DEFAULT_TEXT = CommonMediaType.TEXT_PLAIN;

	/**
	 * Represents the default media type for binary content.
	 */
	public static final CommonMediaType DEFAULT_BINARY = CommonMediaType.APPLICATION_OCTET_STREAM;

	/**
	 * Finds the set of media types corresponding to the given file extension.
	 *
	 * @param extension the file extension to look up (may include the leading dot character)
	 * @return a set of matching {@link CommonMediaType} instances
	 */
	public static Set<CommonMediaType> ofExtension(final String extension) {
		final String ext = (extension.startsWith(".") ? extension.substring(1) : extension).toLowerCase();
		if (ext.isEmpty()) {
			return Set.of();
		}
		final Set<CommonMediaType> mediaTypes = EnumSet.noneOf(CommonMediaType.class);
		for (final CommonMediaType mediaType : values()) {
			if (mediaType.extensions.contains(ext)) {
				mediaTypes.add(mediaType);
			}
		}
		return mediaTypes;
	}

	/**
	 * Finds the set of media types corresponding to the given MIME type.
	 *
	 * @param mimeType the MIME type to look up
	 * @return a set of matching {@link CommonMediaType} instances
	 */
	public static Set<CommonMediaType> ofMimeType(final String mimeType) {
		if (mimeType.isEmpty()) {
			return Set.of();
		}
		final Set<CommonMediaType> mediaTypes = EnumSet.noneOf(CommonMediaType.class);
		for (final CommonMediaType mediaType : values()) {
			if (mediaType.mimeTypes.contains(mimeType)) {
				mediaTypes.add(mediaType);
			}
		}
		return mediaTypes;
	}

	/**
	 * Retrieves the default file extension associated with a given MIME type.
	 * <p>
	 * This method performs a case-insensitive search through the available
	 * {@code CommonMediaType} values to find a matching file extension for the
	 * provided MIME type.
	 * </p>
	 * <p>
	 * Note: This method returns only the default file extension for a MIME type.
	 * If a MIME type can correspond to multiple file extensions, only the first
	 * one encountered will be returned.
	 * </p>
	 * <p>
	 * The extension returned does not include the leading dot (".").
	 * </p>
	 *
	 * @param mimeType the MIME type to look for. The search is case-insensitive
	 * @return the default file extension corresponding to the given MIME type,
	 *         or and empty {@code Optional} if no matching file extension is found
	 */
	public static Optional<String> extensionOf(final @Nullable String mimeType) {
		if (mimeType == null || mimeType.isEmpty()) {
			return Optional.empty();
		}
		for (final CommonMediaType mediaType : CommonMediaType.values()) {
			if (mediaType.getMimeTypes().contains(mimeType.toLowerCase())) {
				return Optional.of(mediaType.getDefaultExtension());
			}
		}
		return Optional.empty();
	}

	/**
	 * Retrieves the default MIME type associated with a given file extension.
	 * <p>
	 * This method performs a case-insensitive search through the available
	 * {@code CommonMediaType} values to find a matching MIME type for the
	 * provided file extension. If the extension starts with a ".", the method
	 * will remove it before performing the search.
	 * </p>
	 * <p>
	 * Note: This method returns only the default MIME type for an extension.
	 * If an extension can correspond to multiple MIME types, only the first
	 * one encountered will be returned.
	 * </p>
	 *
	 * @param extension the file extension to look for. The search is case-insensitive,
	 *            and leading dots are optional (e.g., both "jpg" and ".jpg" are valid)
	 * @return the default MIME type corresponding to the given file extension,
	 *         or an empty {@code Optional} if no matching MIME type is found
	 */
	public static Optional<String> mimeTypeOf(final @Nullable String extension) {
		if (extension == null) {
			return Optional.empty();
		}
		final String ext = (extension.startsWith(".") ? extension.substring(1) : extension).toLowerCase();
		if (ext.isEmpty()) {
			return Optional.empty();
		}
		for (final CommonMediaType mediaType : CommonMediaType.values()) {
			if (mediaType.getExtensions().contains(ext)) {
				return Optional.of(mediaType.getDefaultMimeType());
			}
		}
		return Optional.empty();
	}

	private final List<String> mimeTypes;
	private final List<String> extensions;

	CommonMediaType(final String mimeTypes, final String extensions) {
		this.mimeTypes = List.of(mimeTypes.split(" "));
		this.extensions = List.of(extensions.split(" "));
	}

	/**
	 * Gets the list of MIME types for this media type.
	 *
	 * @return a list of MIME types
	 */
	public List<String> getMimeTypes() {
		return this.mimeTypes;
	}

	/**
	 * Gets the list of file extensions for this media type.
	 *
	 * <p>Note: The extensions are returned without the leading dot (".") character.</p>
	 *
	 * @return a list of file extensions
	 */
	public List<String> getExtensions() {
		return this.extensions;
	}

	/**
	 * Gets the default MIME type for this media type.
	 *
	 * @return the default MIME type
	 */
	public String getDefaultMimeType() {
		return this.mimeTypes.get(0);
	}

	/**
	 * Gets the default file extension for this media type.
	 *
	 * <p>Note: The extension is returned without the leading dot (".") character.</p>
	 *
	 * @return the default file extension
	 */
	public String getDefaultExtension() {
		return this.extensions.get(0);
	}

	/**
	 * Gets the main type (e.g., "text", "image", "audio") of this media type.
	 *
	 * @return the main type
	 */
	public String getType() {
		final String mimeType = this.getDefaultMimeType();
		final int separatorIx = mimeType.indexOf('/');
		return mimeType.substring(0, separatorIx);
	}
}
