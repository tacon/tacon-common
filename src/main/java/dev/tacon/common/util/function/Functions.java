package dev.tacon.common.util.function;

import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;

import dev.tacon.annotations.NonNull;

/**
 * A utility class providing a set of common functions for various operations.
 */
public final class Functions {

	private Functions() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}

	/** A {@link Runnable} that does nothing. */
	public static final @NonNull Runnable NULL_RUNNABLE = () -> {};

	/**
	 * Returns a {@link Supplier} that always supplies {@code null}.
	 *
	 * @param <T> The type of the result
	 * @return A supplier that always returns {@code null}
	 */
	public static @NonNull <T> Supplier<T> nullSupplier() {
		return () -> null;
	}

	/**
	 * Returns a {@link Function} that always returns {@code null}.
	 *
	 * @param <T> The type of the input to the function
	 * @param <R> The type of the result of the function
	 * @return A function that always returns {@code null}
	 */
	public static @NonNull <T, R> Function<T, R> nullFunction() {
		return t -> null;
	}

	/**
	 * Provides a {@link BiFunction} that always returns {@code null}.
	 *
	 * @param <T> The first input type of the function.
	 * @param <U> The second input type of the function.
	 * @param <R> The return type of the function.
	 * @return A bifunction always returning {@code null}.
	 */
	public static @NonNull <T, U, R> BiFunction<T, U, R> nullBiFunction() {
		return (t, u) -> null;
	}

	/**
	 * Provides an {@link IntFunction} that always returns {@code null}.
	 *
	 * @param <R> The return type of the function.
	 * @return An int function always returning {@code null}.
	 */
	public static @NonNull <R> IntFunction<R> nullIntFunction() {
		return i -> null;
	}

	/**
	 * Provides a {@link Consumer} that does nothing.
	 *
	 * @param <T> The generic type.
	 * @return A consumer that does nothing.
	 */
	public static @NonNull <T> Consumer<T> nullConsumer() {
		return t -> {};
	}

	/**
	 * Provides a {@link BiConsumer} that does nothing.
	 *
	 * @param <T> The first type of the biconsumer.
	 * @param <U> The second type of the biconsumer.
	 * @return A biconsumer that does nothing.
	 */
	public static @NonNull <T, U> BiConsumer<T, U> nullBiConsumer() {
		return (t, u) -> {};
	}

	/**
	 * Returns a {@link Predicate} that always returns true.
	 *
	 * @param <T> The input type of the predicate.
	 * @return A predicate that always evaluates to true.
	 */
	public static @NonNull <T> Predicate<T> acceptAll() {
		return t -> true;
	}

	/**
	 * Returns a {@link Predicate} that always returns false.
	 *
	 * @param <T> The input type of the predicate.
	 * @return A predicate that always evaluates to false.
	 */
	public static @NonNull <T> Predicate<T> declineAll() {
		return t -> false;
	}

	/**
	 * Returns a {@link BiPredicate} that always returns true.
	 *
	 * @param <T> The first input type of the bi-predicate.
	 * @param <U> The second input type of the bi-predicate.
	 * @return A bi-predicate that always evaluates to true.
	 */
	public static @NonNull <T, U> BiPredicate<T, U> biAcceptAll() {
		return (t, u) -> true;
	}

	/**
	 * Returns a {@link BiPredicate} that always returns false.
	 *
	 * @param <T> The first input type of the bi-predicate.
	 * @param <U> The second input type of the bi-predicate.
	 * @return A bi-predicate that always evaluates to false.
	 */
	public static @NonNull <T, U> BiPredicate<T, U> biDeclineAll() {
		return (t, u) -> false;
	}

	/**
	 * Returns a {@link BinaryOperator} that always returns its first argument.
	 *
	 * @param <T> The type of the operands and result.
	 * @return A binary operator that always returns its first argument.
	 */
	public static @NonNull <T> BinaryOperator<T> acceptFirst() {
		return (o1, o2) -> o1;
	}

	/**
	 * Returns a {@link BinaryOperator} that always returns its second argument.
	 *
	 * @param <T> The type of the operands and result.
	 * @return A binary operator that always returns its second argument.
	 */
	public static @NonNull <T> BinaryOperator<T> acceptSecond() {
		return (o1, o2) -> o2;
	}

	/**
	 * Returns a {@link BinaryOperator} that returns its first argument if it satisfies the given predicate, otherwise the second argument.
	 *
	 * @param <T> The type of the operands and result.
	 * @param predicate The predicate to test the first argument.
	 * @return A binary operator that returns its first argument if it satisfies the predicate, otherwise the second argument.
	 */
	public static @NonNull <T> BinaryOperator<T> acceptFirstIf(final Predicate<T> predicate) {
		return (o1, o2) -> predicate.test(o1) ? o1 : o2;
	}

	/**
	 * Returns a {@link BinaryOperator} that returns its second argument if it satisfies the given predicate, otherwise the first argument.
	 *
	 * @param <T> The type of the operands and result.
	 * @param predicate The predicate to test the second argument.
	 * @return A binary operator that returns its second argument if it satisfies the predicate, otherwise the first argument.
	 */
	public static @NonNull <T> BinaryOperator<T> acceptSecondIf(final Predicate<T> predicate) {
		return (o1, o2) -> predicate.test(o2) ? o2 : o1;
	}

	/**
	 * Converts a {@link Consumer} to a {@link Function} that applies the consumer and then returns {@code null}.
	 *
	 * @param <T> The type of input to the consumer.
	 * @param consumer The consumer to be converted to a function.
	 * @return A function that applies the consumer and then returns {@code null}.
	 */
	public static @NonNull <T> Function<T, Void> consumerAsFunction(final @NonNull Consumer<T> consumer) {
		return t -> {
			consumer.accept(t);
			return null;
		};
	}

	/**
	 * Returns a function that accepts an input but always returns the result from the given supplier.
	 *
	 * @param <T> The type of the input to the function
	 * @param <X> The type of the result of the function
	 * @param supplier The supplier that provides the result
	 * @return A function that ignores its input and returns the supplier's result
	 */
	public static @NonNull <T, X> Function<T, X> supplierAsFunction(final @NonNull Supplier<X> supplier) {
		return t -> supplier.get();
	}

	/**
	 * Returns a {@link BooleanConsumer} that runs the given {@link Runnable} if the boolean value is {@code true}.
	 *
	 * @param runnable The runnable to execute if the boolean is true
	 * @return A consumer that runs the runnable for true values
	 */
	public static @NonNull BooleanConsumer runIfTrue(final @NonNull Runnable runnable) {
		return b -> {
			if (b) {
				runnable.run();
			}
		};
	}

	// other
	/**
	 * Returns a {@link BinaryOperator} that merges two comparable elements, returning the greater of the two.
	 *
	 * @param <T> The type of the operands, which should be comparable.
	 * @return A binary operator that returns the greater of its two input elements.
	 */
	public static @NonNull <T extends Comparable<? super T>> BinaryOperator<T> mergeGreatest() {
		return mergeGreatest(Function.<T> identity());
	}

	/**
	 * Returns a {@link BinaryOperator} that merges two elements based on the values obtained by applying the provided function.
	 * The operator returns the element corresponding to the greater value.
	 *
	 * @param <T> The type of the operands.
	 * @param <D> The type of the value obtained after applying the function, which should be comparable.
	 * @param function The function to extract a comparable value from the elements.
	 * @return A binary operator that returns the element with the greater extracted value.
	 */
	public static @NonNull <T, D extends Comparable<? super D>> BinaryOperator<T> mergeGreatest(final @NonNull Function<T, D> function) {
		return mergeGreatest(function, Comparator.naturalOrder());
	}

	/**
	 * Returns a {@link BinaryOperator} that merges two elements based on the values obtained by applying the provided function.
	 * The comparison uses a provided comparator. The operator returns the element for which the comparator returns a greater value.
	 *
	 * @param <T> The type of the operands.
	 * @param <D> The type of the value obtained after applying the function.
	 * @param function The function to extract a value from the elements.
	 * @param comparator The comparator used to compare the extracted values.
	 * @return A binary operator that returns the element for which the comparator returns a greater value.
	 */
	public static @NonNull <T, D> BinaryOperator<T> mergeGreatest(final @NonNull Function<T, D> function, final @NonNull Comparator<D> comparator) {
		return (oldVal, newVal) -> comparator.compare(function.apply(oldVal), function.apply(newVal)) > 0 ? oldVal : newVal;
	}

	/**
	 * Returns a {@link BinaryOperator} that merges two comparable elements, returning the lesser of the two.
	 *
	 * @param <T> The type of the operands, which should be comparable.
	 * @return A binary operator that returns the lesser of its two input elements.
	 */
	public static @NonNull <T extends Comparable<? super T>> BinaryOperator<T> mergeLeast() {
		return mergeLeast(Function.<T> identity());
	}

	/**
	 * Returns a {@link BinaryOperator} that merges two elements based on the values obtained by applying the provided function.
	 * The operator returns the element corresponding to the lesser value.
	 *
	 * @param <T> The type of the operands.
	 * @param <D> The type of the value obtained after applying the function, which should be comparable.
	 * @param function The function to extract a comparable value from the elements.
	 * @return A binary operator that returns the element with the lesser extracted value.
	 */
	public static @NonNull <T, D extends Comparable<? super D>> BinaryOperator<T> mergeLeast(final @NonNull Function<T, D> function) {
		return mergeLeast(function, Comparator.naturalOrder());
	}

	/**
	 * Returns a {@link BinaryOperator} that merges two elements based on the values obtained by applying the provided function.
	 * The comparison uses a provided comparator. The operator returns the element for which the comparator returns a lesser value.
	 *
	 * @param <T> The type of the operands.
	 * @param <D> The type of the value obtained after applying the function.
	 * @param function The function to extract a value from the elements.
	 * @param comparator The comparator used to compare the extracted values.
	 * @return A binary operator that returns the element for which the comparator returns a lesser value.
	 */
	public static @NonNull <T, D> BinaryOperator<T> mergeLeast(final @NonNull Function<T, D> function, final @NonNull Comparator<D> comparator) {
		return (oldVal, newVal) -> comparator.compare(function.apply(oldVal), function.apply(newVal)) < 0 ? oldVal : newVal;
	}

	/**
	 * Returns a {@link Predicate} that filters elements based on their distinctness as determined by a key extracted using the provided function.
	 *
	 * @param <T> The type of the input elements.
	 * @param keyExtractor The function to extract a key from the input elements.
	 * @return A predicate that filters elements to ensure distinctness based on the extracted keys.
	 */
	public static @NonNull <T> Predicate<T> distinctByKey(final @NonNull Function<? super T, Object> keyExtractor) {
		final Set<Object> seen = Collections.newSetFromMap(new ConcurrentHashMap<>());
		return t -> seen.add(keyExtractor.apply(t));
	}
}
