package dev.tacon.common.util.function;

/**
 * Represents a function that accepts a {@code boolean}-valued argument and produces
 * a result of type {@code R}. This is a {@code boolean}-consuming primitive specialization
 * for {@link java.util.function.Function}.
 *
 * <p>This is a functional interface whose functional method is {@link #apply(boolean)}.
 *
 * @param <R> the type of the result of the function
 *
 * @see java.util.function.Function
 */
@FunctionalInterface
public interface BooleanFunction<R> {

	/**
	 * Applies this function to the given {@code boolean} argument.
	 *
	 * @param value the {@code boolean} input argument
	 * @return the function result
	 */
	R apply(boolean value);
}
