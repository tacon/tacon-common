package dev.tacon.common.util;

import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;
import static java.util.Comparator.nullsLast;
import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Optional;

import dev.tacon.annotations.NonNull;

/**
 * Represents an immutable range of values, defined by a minimum and a maximum value.
 * The range can be bounded or unbounded on either end.
 *
 * @param <C> the type of element contained in the range, which must be comparable or have a provided comparator.
 */
public class Range<C> implements Comparable<Range<C>>, Serializable {

	private static final long serialVersionUID = -7554577129972955934L;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static final Range INFINITE = new Range((o1, o2) -> 0, null, null);

	private final Comparator<C> comparator;
	private final C min;
	private final C max;

	/**
	 * Creates a range with specified minimum and maximum values using the provided comparator.
	 *
	 * @param comparator the comparator to use for ordering the range values.
	 * @param min the minimum value of the range.
	 * @param max the maximum value of the range.
	 * @return a new range instance.
	 */
	public static @NonNull <C> Range<C> between(final @NonNull Comparator<C> comparator, final C min, final C max) {
		return new Range<>(comparator, min, max);
	}

	/**
	 * Creates a range with specific minimum and maximum values using natural ordering.
	 *
	 * @param min the minimum value of the range.
	 * @param max the maximum value of the range.
	 * @return a new range instance.
	 */
	public static @NonNull <C extends Comparable<? super C>> Range<C> between(final C min, final C max) {
		return between(naturalOrder(), min, max);
	}

	/**
	 * Creates a range starting from the specified minimum value, using the provided comparator.
	 * The end of this range will be unbounded.
	 *
	 * @param comparator the comparator to use for ordering the range values.
	 * @param min the minimum value from where the range starts.
	 * @return a new range instance that starts from the specified value and has no bounded end.
	 */
	public static @NonNull <C> Range<C> starting(final @NonNull Comparator<C> comparator, final C min) {
		return new Range<>(comparator, min, null);
	}

	/**
	 * Creates a range starting from the specified minimum value, using natural ordering.
	 * The end of this range will be unbounded.
	 *
	 * @param min the minimum value from where the range starts.
	 * @return a new range instance that starts from the specified value and has no bounded end.
	 */
	public static @NonNull <C extends Comparable<? super C>> Range<C> starting(final C min) {
		return starting(naturalOrder(), min);
	}

	/**
	 * Creates a range ending at the specified maximum value, using the provided comparator.
	 * The start of this range will be unbounded.
	 *
	 * @param comparator the comparator to use for ordering the range values.
	 * @param max the maximum value where the range ends.
	 * @return a new range instance that has no bounded start and ends at the specified value.
	 */
	public static @NonNull <C> Range<C> ending(final @NonNull Comparator<C> comparator, final C max) {
		return new Range<>(comparator, null, max);
	}

	/**
	 * Creates a range ending at the specified maximum value, using natural ordering.
	 * The start of this range will be unbounded.
	 *
	 * @param max the maximum value where the range ends.
	 * @return a new range instance that has no bounded start and ends at the specified value.
	 */
	public static @NonNull <C extends Comparable<? super C>> Range<C> ending(final C max) {
		return ending(naturalOrder(), max);
	}

	/**
	 * Creates a range representing a single point, using the provided comparator.
	 * The start and end of this range will be the specified point.
	 *
	 * @param comparator the comparator to use for ordering the range values.
	 * @param obj the value representing the single point in the range.
	 * @return a new range instance representing the specified point.
	 */
	public static @NonNull <C> Range<C> point(final @NonNull Comparator<C> comparator, final C obj) {
		return new Range<>(comparator, obj, obj);
	}

	/**
	 * Creates a range representing a single point, using natural ordering.
	 * The start and end of this range will be the specified point.
	 *
	 * @param obj the value representing the single point in the range.
	 * @return a new range instance representing the specified point.
	 */
	public static @NonNull <C extends Comparable<? super C>> Range<C> point(final C obj) {
		return point(naturalOrder(), obj);
	}

	/**
	 * Provides a range that represents an infinite range.
	 * Both the start and end of this range will be unbounded.
	 *
	 * @return a range instance that has no bounds on either end.
	 */
	@SuppressWarnings("unchecked")
	public static @NonNull <C> Range<C> infinite() {
		return INFINITE;
	}

	/**
	 * Checks if the range is valid based on the provided minimum and maximum values using natural ordering.
	 *
	 * @param min the minimum value.
	 * @param max the maximum value.
	 * @return true if the range is valid, false otherwise.
	 */
	public static <C extends Comparable<? super C>> boolean isValid(final C min, final C max) {
		return isValid(min, max, naturalOrder());
	}

	/**
	 * Checks if a range is valid based on provided minimum and maximum values and a comparator.
	 *
	 * @param min the minimum value.
	 * @param max the maximum value.
	 * @param comparator the comparator to use for ordering.
	 * @return true if the range is valid, false otherwise.
	 */
	public static <C> boolean isValid(final C min, final C max, final @NonNull Comparator<C> comparator) {
		requireNonNull(comparator, "Comparator cannot be null");
		return min == null || max == null || comparator.compare(min, max) <= 0;
	}

	private Range(final @NonNull Comparator<C> comparator, final C min, final C max) {
		this.comparator = requireNonNull(comparator, "Comparator cannot be null");

		if (!isValid(min, max, comparator)) {
			throw new IllegalArgumentException("Min value cannot be greater than Max value: %s > %s".formatted(min, max));
		}
		this.min = min;
		this.max = max;
	}

	/**
	 * Determines if the specified value is within the range (exclusive of boundaries).
	 *
	 * @param value the value to check.
	 * @return true if the value is within the range excluding the boundaries, false otherwise.
	 */
	public boolean containsExclusive(final C obj) {
		return this.contains(obj, true, true);
	}

	/**
	 * Determines if the specified value is within the range (inclusive of boundaries).
	 *
	 * @param value the value to check.
	 * @return true if the value is within the range including the boundaries, false otherwise.
	 */
	public boolean containsInclusive(final C obj) {
		return this.contains(obj, false, false);
	}

	/**
	 * Determines if the specified value is within the range.
	 *
	 * @param value the value to check.
	 * @return true if the value is within the range, false otherwise.
	 */
	public boolean contains(final C obj) {
		return this.contains(obj, false, true);
	}

	/**
	 * Determines whether the given value lies within the range.
	 *
	 * @param obj the value to check.
	 * @param excludeMin if true, excludes the minimum value from the range check.
	 * @param excludeMax if true, excludes the maximum value from the range check.
	 * @return true if the value lies within the specified range, false otherwise.
	 */
	public boolean contains(final C obj, final boolean excludeMin, final boolean excludeMax) {
		if (obj == null) {
			return false;
		}

		final int minFactor = excludeMin ? -1 : 0;
		final int maxFactor = excludeMax ? 1 : 0;

		if (this.min == null && this.max == null) {
			return true;
		}
		if (this.min == null) {
			return this.comparator.compare(this.max, obj) >= maxFactor;
		}
		if (this.max == null) {
			return this.comparator.compare(this.min, obj) <= minFactor;
		}
		return this.comparator.compare(this.min, obj) <= minFactor
				&& this.comparator.compare(this.max, obj) >= maxFactor;
	}

	/**
	 * Checks if the given point lies before the range.
	 *
	 * @param obj the point to check.
	 * @return true if the point lies before the range, false otherwise.
	 */
	public boolean isBefore(final C obj) {
		if (obj == null || this.max == null) {
			return false;
		}
		return this.comparator.compare(this.max, obj) < 0;
	}

	/**
	 * Checks if the given point lies after the range.
	 *
	 * @param obj the point to check.
	 * @return true if the point lies after the range, false otherwise.
	 */
	public boolean isAfter(final C obj) {
		if (obj == null || this.min == null) {
			return false;
		}
		return this.comparator.compare(this.min, obj) > 0;
	}

	/**
	 * Checks if the current range starts before the given range.
	 *
	 * @param range the range to compare against.
	 * @return true if the current range starts before the given range, false otherwise.
	 */
	public boolean startsBefore(final @NonNull Range<C> range) {
		requireNonNull(range, "Range cannot be null");
		if (this.min == null) {
			return range.min != null;
		}
		if (range.min == null) {
			return false;
		}
		return this.comparator.compare(this.min, range.min) < 0;
	}

	/**
	 * Checks if the current range ends before the given range.
	 *
	 * @param range the range to compare against.
	 * @return true if the current range ends before the given range, false otherwise.
	 */
	public boolean endsBefore(final @NonNull Range<C> range) {
		requireNonNull(range, "Range cannot be null");
		if (this.max == null) {
			return false;
		}
		if (range.max == null) {
			return this.max != null;
		}
		return this.comparator.compare(this.max, range.max) < 0;
	}

	/**
	 * Checks if the current range overlaps with the specified range defined by its minimum and maximum values.
	 *
	 * @param otherMin the minimum value of the range to check against.
	 * @param otherMax the maximum value of the range to check against.
	 * @return true if the current range overlaps with the specified range, false otherwise.
	 */
	public boolean isOverlapping(final C otherMin, final C otherMax) {
		return (this.max == null || otherMin == null || this.comparator.compare(this.max, otherMin) >= 0)
				&& (this.min == null || otherMax == null || this.comparator.compare(this.min, otherMax) <= 0);
	}

	/**
	 * Checks if the current range overlaps with the given range.
	 *
	 * @param range the range to check against.
	 * @return true if the current range overlaps with the specified range, false otherwise.
	 */
	public boolean isOverlapping(final @NonNull Range<C> range) {
		requireNonNull(range, "Range cannot be null");
		return this.isOverlapping(range.min, range.max);
	}

	/**
	 * Determines the intersection of the current range with the given range.
	 *
	 * @param range the range to intersect with.
	 * @return an {@code Optional} containing the intersecting range,
	 *         or an empty {@code Optional} if the ranges don't overlap.
	 */
	public Optional<Range<C>> intersect(final @NonNull Range<C> range) {
		requireNonNull(range, "Range cannot be null");

		final C iMin = nullsFirst(this.comparator).compare(this.min, range.min) >= 0 ? this.min : range.min;
		final C iMax = nullsLast(this.comparator).compare(this.max, range.max) <= 0 ? this.max : range.max;

		if (iMin != null && iMax != null && this.comparator.compare(iMin, iMax) > 0) {
			return Optional.empty();
		}
		return Optional.of(new Range<>(this.comparator, iMin, iMax));
	}

	/**
	 * Checks if the current range represents a single point (start and end are equal).
	 *
	 * @return true if the range represents a single point, false otherwise.
	 */
	public boolean isPoint() {
		return this.min != null && this.max != null
				&& this.comparator.compare(this.min, this.max) == 0;
	}

	/**
	 * Determines if both the start and end of this range are bounded (i.e., not infinite).
	 *
	 * @return true if both boundaries are bounded, false otherwise.
	 */
	public boolean isBounded() {
		return this.min != null && this.max != null;
	}

	/**
	 * Checks if the range is infinite (unbounded on both ends).
	 *
	 * @return true if the range is infinite, false otherwise.
	 */
	public boolean isInfinite() {
		return this.min == null && this.max == null;
	}

	/**
	 * Retrieves the minimum value of the range.
	 *
	 * @return the minimum value if the range is bounded at the start; null if unbounded.
	 */
	public C getMin() {
		return this.min;
	}

	/**
	 * Retrieves the maximum value of the range.
	 *
	 * @return the maximum value if the range is bounded at the end; null if unbounded.
	 */
	public C getMax() {
		return this.max;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.max == null ? 0 : this.max.hashCode());
		result = prime * result + (this.min == null ? 0 : this.min.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		@SuppressWarnings("unchecked")
		final Range<C> other = (Range<C>) obj;
		if (this.max == null) {
			if (other.max != null) {
				return false;
			}
		} else if (!this.max.equals(other.max)) {
			return false;
		}
		if (this.min == null) {
			if (other.min != null) {
				return false;
			}
		} else if (!this.min.equals(other.min)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "{" + this.min + " - " + this.max + "}";
	}

	@Override
	public int compareTo(final Range<C> o) {
		if (this.equals(o)) {
			return 0;
		}
		int c = nullsFirst(this.comparator).compare(this.min, o.min);
		if (c == 0) {
			c = nullsLast(this.comparator).compare(this.max, o.max);
		}
		return c;
	}
}
