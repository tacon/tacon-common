package dev.tacon.common.time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import dev.tacon.annotations.NonNull;

/**
 * Utility class to provide commonly used date and time operations.
 */
public final class DateTimeUtils {

	/**
	 * Converts a {@link LocalDateTime} from UTC to an {@link OffsetDateTime}.
	 *
	 * @param dateTime the LocalDateTime to convert
	 * @return an OffsetDateTime with UTC offset,
	 *         or {@code null} if the input was null
	 */
	public static OffsetDateTime toOffsetDateTimeFromUTC(final LocalDateTime dateTime) {
		return toOffsetDateTime(dateTime, ZoneOffset.UTC, ZoneOffset.UTC);
	}

	/**
	 * Converts a {@link LocalDateTime} from a source zone offset to a target {@link OffsetDateTime} with a target zone offset.
	 *
	 * @param dateTime the LocalDateTime to convert
	 * @param sourceZoneOffset the original zone offset of the dateTime
	 * @param targetZoneOffset the target zone offset for the resulting OffsetDateTime
	 * @return an OffsetDateTime with the target zone offset,
	 *         or {@code null} if the input was null
	 */
	public static OffsetDateTime toOffsetDateTime(final LocalDateTime dateTime, final ZoneOffset sourceZoneOffset, final ZoneOffset targetZoneOffset) {
		return dateTime != null ? OffsetDateTime.ofInstant(dateTime.toInstant(sourceZoneOffset), targetZoneOffset) : null;
	}

	/**
	 * Converts an {@link OffsetDateTime} to a {@link LocalDateTime} with UTC offset.
	 *
	 * @param offsetDateTime the OffsetDateTime to convert
	 * @return a LocalDateTime with UTC offset,
	 *         or {@code null} if the input was null
	 */
	public static LocalDateTime toLocalDateTimeUTC(final OffsetDateTime offsetDateTime) {
		return toLocalDateTime(offsetDateTime, ZoneOffset.UTC);
	}

	/**
	 * Converts an {@link OffsetDateTime} to a {@link LocalDateTime} with a target zone offset.
	 *
	 * @param offsetDateTime the OffsetDateTime to convert
	 * @param targetZoneOffset the target zone offset for the resulting LocalDateTime
	 * @return a LocalDateTime with the target zone offset,
	 *         or {@code null} if the input was null
	 */
	public static LocalDateTime toLocalDateTime(final OffsetDateTime offsetDateTime, final ZoneOffset targetZoneOffset) {
		return offsetDateTime != null ? offsetDateTime.atZoneSameInstant(targetZoneOffset).toLocalDateTime() : null;
	}

	/**
	 * Adjusts the provided {@link LocalDate} to the last day of its month.
	 *
	 * @param date the date to adjust
	 * @return a new LocalDate adjusted to the last day of its month,
	 *         or {@code null} if the input was null
	 */
	public static LocalDate toLastDayOfMonth(final LocalDate date) {
		return date == null ? null : date.withDayOfMonth(date.lengthOfMonth());
	}

	/**
	 * Calculates the age between a given birth date and a reference date.
	 *
	 * @param birthDay the birth date
	 * @param when the reference date
	 * @return the age in years between the birth date and the reference date
	 */
	public static int age(final @NonNull LocalDate birthDay, final @NonNull LocalDate when) {
		return (int) birthDay.until(when, ChronoUnit.YEARS);
	}

	/**
	 * Determines if the provided "when" date is the birthday based on the given birth date.
	 *
	 * @param birthdate the birth date
	 * @param when the date to check
	 * @return true if it's the birthday, false otherwise
	 */
	public static boolean isBirthday(final @NonNull LocalDate birthdate, final @NonNull LocalDate when) {
		if (birthdate.getMonth() == when.getMonth() && birthdate.getDayOfMonth() == when.getDayOfMonth()) {
			return true;
		}
		if (birthdate.getMonth() == Month.FEBRUARY && birthdate.getDayOfMonth() == 29) {
			if (!when.isLeapYear() && when.getMonth() == Month.FEBRUARY && when.getDayOfMonth() == 28) {
				return true;
			}
		}
		return false;
	}

	private DateTimeUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
