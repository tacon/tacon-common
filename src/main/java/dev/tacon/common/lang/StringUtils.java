package dev.tacon.common.lang;

import static java.util.Objects.requireNonNull;

import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.function.Function;
import java.util.regex.Pattern;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.NonNullByDefault;

/**
 * Utility class for performing various string manipulations.
 */
public final class StringUtils {

	private static final String ASCII_CHARSET = "ascii";
	private static final String[] HTML_CODES = new String[256];
	static {
		for (int i = 0; i < 10; i++) {
			HTML_CODES[i] = "&#00" + i + ";";
		}
		for (int i = 10; i < 32; i++) {
			HTML_CODES[i] = "&#0" + i + ";";
		}
		for (int i = 32; i < 128; i++) {
			HTML_CODES[i] = String.valueOf((char) i);
		}
		HTML_CODES['\t'] = "\t";
		HTML_CODES['\n'] = "<br />\n";
		HTML_CODES['\"'] = "&quot;";
		HTML_CODES['&'] = "&amp;";
		HTML_CODES['<'] = "&lt;";
		HTML_CODES['>'] = "&gt;";
	}

	// null/empty/blank

	/**
	 * Checks if a {@code CharSequence} is null or empty.
	 *
	 * @param cs the character sequence to check.
	 * @return true if cs is null or its length is 0, false otherwise.
	 */
	public static boolean isNullOrEmpty(final CharSequence cs) {
		return cs == null || cs.length() == 0;
	}

	/**
	 * Checks if a {@code CharSequence} is not null and not empty.
	 *
	 * @param cs the character sequence to check.
	 * @return true if cs is not null and not empty, false otherwise.
	 */
	public static boolean isNotNullNorEmpty(final CharSequence s) {
		return s != null && s.length() > 0;
	}

	/**
	 * Checks if a {@code String} is null or blank.
	 *
	 * @param cs the string to check.
	 * @return true if cs is null or its length is 0, false otherwise.
	 */
	public static boolean isNullOrBlank(final String s) {
		return s == null || s.isBlank();
	}

	/**
	 * Checks if a {@code String} is not null and not blank.
	 *
	 * @param cs the string to check.
	 * @return true if cs is not null and not blank, false otherwise.
	 */
	public static boolean isNotNullNorBlank(final String s) {
		return s != null && !s.isBlank();
	}

	/**
	 * Converts an empty string to {@code null}.
	 *
	 * <p>This method takes a string {@code s} as input and returns {@code null} if the string is empty.
	 * Otherwise, it returns the original string.</p>
	 *
	 * @param s the source string to be converted
	 * @return {@code null} if the input string is empty, otherwise the original string
	 */
	public static String emptyToNull(final String s) {
		return "".equals(s) ? null : s;
	}

	/**
	 * Converts an blank string to {@code null}.
	 *
	 * <p>This method takes a string {@code s} as input and returns {@code null} if the string is blank.
	 * Otherwise, it returns the original string.</p>
	 *
	 * @param s the source string to be converted
	 * @return {@code null} if the input string is blank, otherwise the original string
	 */
	public static String blankToNull(final String s) {
		return s == null || s.isBlank() ? null : s;
	}

	/**
	 * Converts a {@code null} string to an empty string ("").
	 *
	 * <p>This method takes a string {@code s} as input and returns an empty string if the string is {@code null}.
	 * Otherwise, it returns the original string.</p>
	 *
	 * @param s the source string to be converted
	 * @return an empty string if the input string is {@code null}, otherwise the original string
	 */
	@NonNull
	public static String nullToEmpty(final String s) {
		return s == null ? "" : s;
	}

	/**
	 * Strips whitespaces from both ends of the given string and converts empty strings to {@code null}.
	 *
	 * @param s the source string to be processed
	 * @return a stripped string if the input string contains characters other than whitespaces;
	 *         {@code null} if the input string is either {@code null} or only contains whitespaces
	 */
	public static String stripToNull(final String s) {
		if (s == null) {
			return null;
		}
		final String stripped = s.strip();
		return stripped.isEmpty() ? null : stripped;
	}

	/**
	 * Strips whitespaces from both ends of the given string and converts {@code null} strings to an empty string.
	 *
	 * @param s the source string to be processed
	 * @return a stripped string if the input string contains characters other than whitespaces;
	 *         an empty string ("") if the input string is either {@code null} or only contains whitespaces
	 */
	public static @NonNull String stripToEmpty(final String s) {
		return s == null ? "" : s.strip();
	}

	// insertion/replace/deletion

	/**
	 * Inserts a given string at a specified position in the source string.
	 *
	 * <p><strong>Example Usage:</strong></p>
	 *
	 * <pre>{@code
	 *
	 * String original = "loWorld";
	 * String result = insert(original, "Hel", 0);
	 * }</pre>
	 *
	 * In this example, {@code result} will contain "HelloWorld".
	 *
	 * @param s the source string in which to insert the new string
	 * @param strToInsert the string to insert into {@code s}
	 * @param index the position at which to insert {@code strToInsert}
	 *
	 * @return a new string with {@code strToInsert} inserted at index {@code pos}
	 *
	 * @throws NullPointerException if either {@code s} or {@code strToInsert} is {@code null}
	 * @throws StringIndexOutOfBoundsException if {@code pos} is out of range for the length of {@code s}
	 */
	@NonNullByDefault
	public static String insert(final String s, final String strToInsert, final int index) {
		final int sLen = requireNonNull(s, "Source string required").length();
		requireNonNull(strToInsert, "String to insert required");
		if (index > sLen) {
			throw new StringIndexOutOfBoundsException(index + " > " + s.length());
		}
		if (index == 0) {
			return strToInsert + s;
		}
		if (index == sLen) {
			return s.substring(0, index) + strToInsert;
		}
		return s.substring(0, index) + strToInsert + s.substring(index, sLen);
	}

	/**
	 * Removes a substring from the source string based on the specified range.
	 *
	 * <p><strong>Example Usage:</strong></p>
	 *
	 * <pre>{@code
	 *
	 * String original = "HelloWorld";
	 * String result = remove(original, 0, 5);
	 * }</pre>
	 *
	 * In this example, {@code result} will contain "World".
	 *
	 * @param s the source string from which to remove the substring
	 * @param beginIndex the start index of the range to remove from {@code s}
	 * @param endIndex the end index of the range to remove from {@code s}
	 *
	 * @return a new string with the specified range removed
	 *
	 * @throws NullPointerException if the source string {@code s} is {@code null}
	 */
	@NonNullByDefault
	public static String remove(final String s, final int beginIndex, final int endIndex) {
		final int sLen = requireNonNull(s, "Source string required").length();
		if (beginIndex == 0) {
			return sLen == endIndex ? "" : s.substring(endIndex, sLen);
		}
		if (sLen == endIndex) {
			return s.substring(0, beginIndex);
		}
		return s.substring(0, beginIndex) + s.substring(endIndex, sLen);
	}

	/**
	 * Replaces a substring within the source string using a replace function.
	 * The substring to be replaced starts at the index {@code startIndex} and ends at the index {@code endIndex}.
	 *
	 * <p>The {@code replaceFunction} takes the substring to be replaced as an argument and returns the replacement string.</p>
	 *
	 * <p><strong>Example:</strong></p>
	 *
	 * <pre>{@code
	 *
	 * String str = "Hello, World!";
	 * String result = replace(str, 7, 12, String::toUpperCase);
	 * }</pre>
	 *
	 * In this example, {@code result} will contain "Hello, WORLD!".
	 *
	 * @param s the source string.
	 * @param beginIndex the starting index of the substring to be replaced, inclusive
	 * @param endIndex the ending index of the substring to be replaced, exclusive
	 * @param replaceFunction the function to generate the replacement string
	 *
	 * @return the string with the substring replaced as per the {@code replaceFunction}
	 *
	 * @throws NullPointerException if the source string or the replace function is {@code null}
	 * @throws IllegalArgumentException if {@code from} is greater than {@code to}
	 * @throws IndexOutOfBoundsException if {@code from} or {@code to} is out of range
	 */
	@NonNullByDefault
	public static String replace(final String s, final int beginIndex, final int endIndex, final Function<String, String> replaceFunction) {
		requireNonNull(s, "Source string required");
		if (beginIndex == endIndex) {
			return s;
		}
		final int sLen = s.length();
		if (beginIndex > endIndex) {
			throw new IllegalArgumentException("Invalid range: " + beginIndex + " > " + endIndex);
		}
		requireNonNull(replaceFunction, "Replace function required");
		if (beginIndex == 0) {
			return sLen == endIndex ? replaceFunction.apply(s) : replaceFunction.apply(s.substring(beginIndex, endIndex)) + s.substring(endIndex, sLen);
		}
		if (sLen == endIndex) {
			return s.substring(0, beginIndex) + replaceFunction.apply(s.substring(beginIndex, endIndex));
		}
		return s.substring(0, beginIndex) + replaceFunction.apply(s.substring(beginIndex, endIndex)) + s.substring(endIndex, sLen);
	}

	/**
	 * Removes all instances of a specified character from the beginning of a given string.
	 *
	 * <p>This method takes a string {@code s} and a character {@code c} as input and removes all occurrences
	 * of {@code c} that are leading (i.e., at the beginning) in the string.</p>
	 *
	 * @param s the source string to be processed, not null
	 * @param c the character to be removed from the beginning of the source string
	 * @return a new string with all leading instances of {@code c} removed
	 * @throws NullPointerException if the input string is null
	 */
	@NonNullByDefault
	public static String removeLeadingChars(final String s, final char c) {
		if (requireNonNull(s, "Source string required").isEmpty()) {
			return s;
		}
		int from = 0;
		int i = 0;
		while (i < s.length() && s.charAt(i++) == c) {
			from++;
		}
		return s.substring(from);
	}

	/**
	 * Removes all instances of a specified character from the end of a given string.
	 *
	 * <p>This method takes a string {@code s} and a character {@code c} as input and removes all occurrences
	 * of {@code c} that are trailing (i.e., at the end) in the string.</p>
	 *
	 * @param s the source string to be processed, not null
	 * @param c the character to be removed from the end of the source string
	 * @return a new string with all trailing instances of {@code c} removed
	 * @throws NullPointerException if the input string is null.
	 */
	@NonNullByDefault
	public static String removeTrailingChars(final String s, final char c) {
		if (requireNonNull(s, "Source string required").isEmpty()) {
			return s;
		}
		int to = s.length();
		int i = to - 1;
		while (i >= 0 && s.charAt(i--) == c) {
			to--;
		}
		return s.substring(0, to);
	}

	// misc

	/**
	 * Capitalizes the first letter of each word in a string.
	 *
	 * @param s the string to capitalize.
	 * @return the capitalized string.
	 */
	@NonNullByDefault
	public static String capitalize(final String s) {
		final int length = requireNonNull(s, "Source string required").length();
		if (length == 0) {
			return s;
		}
		if (length == 1) {
			return String.valueOf(Character.toUpperCase(s.charAt(0)));
		}
		final char[] chars = s.toCharArray();
		final char[] result = new char[length];
		boolean upper = true;
		int i = 0;
		for (; i < length - 1; i++) {
			final char c = chars[i];
			if (!Character.isLetter(c)) {
				upper = true;
				result[i] = c;
			} else {
				result[i] = upper && !Character.isWhitespace(chars[i + 1]) ? Character.toUpperCase(c) : c;
				upper = false;
			}
		}
		result[i] = chars[i];
		return new String(result);
	}

	/**
	 * Generates a new string consisting of the given character repeated for the specified length.
	 *
	 * @param character the character to repeat
	 * @param length the length of the resulting string
	 * @return a string made up of {@code length} instances of {@code character}.
	 *         Returns an empty string if {@code length} is 0
	 * @throws IllegalArgumentException if {@code length} is negative
	 */
	@NonNull
	public static String generate(final char character, final int length) {
		if (length == 0) {
			return "";
		}
		if (length == 1) {
			return String.valueOf(character);
		}
		final char[] array = new char[length];
		Arrays.fill(array, character);
		return new String(array);
	}

	/**
	 * Generates a new string consisting of the given string repeated for the specified times.
	 *
	 * @param string the string to repeat
	 * @param repetitions the number of repetitions
	 * @return a string made up of {@code repetitions} instances of {@code string}.
	 *         Returns an empty string if {@code repetitions} is 0
	 * @throws IllegalArgumentException if {@code repetitions} is negative
	 */
	@NonNullByDefault
	public static String generate(final String string, final int repetitions) {
		requireNonNull(string, "String to repeat required");
		if (repetitions < 0) {
			throw new IllegalArgumentException("Negative number of repetitions");
		}
		if (repetitions == 0 || string.isEmpty()) {
			return "";
		}
		if (repetitions == 1) {
			return string;
		}
		final StringBuilder result = new StringBuilder(string.length() * repetitions);
		for (int i = 0; i < repetitions; i++) {
			result.append(string);
		}
		return result.toString();
	}

	/**
	 * Returns the leftmost {@code n} characters from the input string.
	 *
	 * <p>If {@code n} is greater than the length of the string, the entire string is returned.
	 * If the input string is {@code null}, an empty string is returned.</p>
	 *
	 * <pre>
	 * left("ciao", 2) -> "ci"
	 * left("ciao", 7) -> "ciao"
	 * left(null, 4) -> ""
	 * </pre>
	 *
	 * @param s the input string from which to extract the leftmost characters.
	 * @param n the number of characters to extract.
	 *
	 * @return the leftmost {@code n} characters of the input string.
	 */
	public static @NonNull String left(final String s, final int n) {
		final String strNotNull = nullToEmpty(s);
		if (strNotNull.length() > n) {
			return strNotNull.substring(0, n);
		}
		return strNotNull;
	}

	/**
	 * Returns the rightmost {@code n} characters from the input string.
	 *
	 * <p>If {@code n} is greater than the length of the string, the entire string is returned.
	 * If the input string is {@code null}, an empty string is returned.</p>
	 *
	 * <pre>
	 * right("ciao", 2) -> "ao"
	 * right("ciao", 7) -> "ciao"
	 * right(null, 4) -> ""
	 * </pre>
	 *
	 *
	 * @param s the input string from which to extract the rightmost characters.
	 * @param n the number of characters to extract.
	 *
	 * @return the rightmost {@code n} characters of the input string.
	 */
	public static @NonNull String right(final String s, final int n) {
		final String strNotNull = nullToEmpty(s);
		if (strNotNull.length() > n) {
			return strNotNull.substring(strNotNull.length() - n);
		}
		return strNotNull;
	}

	/**
	 * Pads the given string on the left with a specified padding character until it reaches a specified length.
	 *
	 * <p>If the length of the string is greater than the specified length, the string is truncated from the right.</p>
	 *
	 * <pre>
	 * padLeft("ciao", 6, '*') -> '**ciao'
	 * padLeft("ciao", 2, '*') -> 'ci'
	 * padLeft("", 3, '*') -> '***'
	 * </pre>
	 *
	 *
	 * @param s the string to pad.
	 * @param length the desired length of the resulting string.
	 * @param paddingChar the character used for padding.
	 *
	 * @return the padded string of the specified length.
	 * @throws NullPointerException if the input string is null.
	 */
	@NonNullByDefault
	public static String padLeft(final String s, final int length, final char paddingChar) {
		final int sLength = requireNonNull(s, "Source string required").length();

		final int diff = length - sLength;
		if (diff == 0) {
			return s;
		}
		if (diff < 0) {
			return s.substring(0, length);
		}
		return generate(paddingChar, diff) + s;
	}

	/**
	 * Pads the given string on the right with a specified padding character until it reaches a specified length.
	 *
	 * <p>If the length of the string is greater than the specified length, the string is truncated from the right.</p>
	 *
	 * <pre>
	 * padRight("ciao", 6, '*') -> 'ciao**'
	 * padRight("ciao", 2, '*') -> 'ci'
	 * padRight("", 3, '*') -> '***'
	 * </pre>
	 *
	 *
	 * @param s the string to pad.
	 * @param length the desired length of the resulting string.
	 * @param paddingChar the character used for padding.
	 *
	 * @return the padded string of the specified length.
	 * @throws NullPointerException if the input string is null.
	 */
	@NonNullByDefault
	public static String padRight(final String s, final int length, final char paddingChar) {
		final int diff = length - requireNonNull(s, "Source string required").length();
		if (diff == 0) {
			return s;
		}
		if (diff < 0) {
			return s.substring(0, length);
		}
		return s + generate(paddingChar, diff);
	}

	/**
	 * Checks if a given string contains a specified substring, case-insensitively.
	 *
	 * @param source the source string to search within.
	 * @param target the substring to search for.
	 *
	 * @return {@code true} if the source string contains the target substring, ignoring case;
	 *         {@code false} otherwise.
	 */
	public static boolean containsIgnoreCase(final String source, final String search) {
		if (source == null || search == null) {
			return false;
		}
		final int searchLength = search.length();
		for (int i = 0; i <= source.length() - searchLength; i++) {
			if (source.regionMatches(true, i, search, 0, searchLength)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Finds the common prefix between two strings. This method compares the characters
	 * in both strings from the beginning until the first mismatch, and returns the common prefix.
	 *
	 * <p><strong>Example:</strong></p>
	 *
	 * <pre>{@code
	 *
	 * String str1 = "apple";
	 * String str2 = "appetizer";
	 * String commonPrefix = StringUtils.findCommonPrefix(str1, str2);
	 * }</pre>
	 *
	 * In this example, {@code commonPrefix} will contain "app".
	 *
	 * @param s1 the first string to be compared.
	 * @param s2 the second string to be compared.
	 *
	 * @returnthe common prefix between the two strings,
	 *            or {@code null} if either of the input strings is {@code null}
	 *            or if there is no common prefix.
	 */
	public static String findCommonPrefix(final String s1, final String s2) {
		if (s1 == null || s2 == null) {
			return null;
		}
		int i;
		for (i = 0; i < s1.length() && i < s2.length(); i++) {
			if (s1.charAt(i) != s2.charAt(i)) {
				break;
			}
		}
		return i > 0 ? s1.substring(0, i) : null;
	}

	/**
	 * Encodes a given string for safe HTML rendering by replacing special HTML characters
	 * with their respective HTML entities. For example, '<' will be replaced with "&lt;", '>' with "&gt;", etc.
	 * This method also handles ASCII characters outside the printable range by converting them into
	 * appropriate HTML numeric character references.
	 *
	 * <p><strong>Example:</strong></p>
	 *
	 * <pre>{@code
	 *
	 * String unsafe = "<script>alert('unsafe');</script>";
	 * String safe = StringUtils.encodeHTML(unsafe);
	 * }</pre>
	 *
	 * After encoding, {@code safe} will contain {@code "&lt;script&gt;alert(&#39;unsafe&#39;);&lt;/script&gt;"}.
	 *
	 * @param s the input string that needs to be HTML-encoded.
	 * @return the HTML-encoded representation of the input string. If the input is an empty string or null,
	 *         an empty string is returned.
	 *
	 * @throws NullPointerException if the input string is null.
	 */
	@NonNullByDefault
	public static String encodeHTML(final String s) {
		if (requireNonNull(s, "Source string required").isEmpty()) {
			return s;
		}
		final StringBuilder result = new StringBuilder();
		for (final char character : s.toCharArray()) {
			if (character < 128) {
				result.append(HTML_CODES[character]);
			} else {
				result.append("&#").append((int) character).append(";");
			}
		}
		return result.toString().trim();

	}

	/**
	 * Encodes a string in ASCII format.
	 *
	 * @param s the string to encode.
	 * @return the ASCII-encoded string.
	 *
	 * @throws NullPointerException if the input string is null.
	 */
	@NonNullByDefault
	public static String encodeASCII(final String s) {
		if (requireNonNull(s, "Source string required").isEmpty()) {
			return s;
		}
		final String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		final Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		try {
			return new String(pattern.matcher(temp).replaceAll("").getBytes(ASCII_CHARSET), ASCII_CHARSET);
		} catch (final UnsupportedEncodingException ex) {
			assert false : ex;
			return s;
		}
	}

	private StringUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
