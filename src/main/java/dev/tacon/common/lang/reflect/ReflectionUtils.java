package dev.tacon.common.lang.reflect;

import java.lang.reflect.Method;

import dev.tacon.annotations.NonNull;

/**
 * Utility class for simplifying reflection-based operations.
 */
public final class ReflectionUtils {

	/**
	 * Finds and returns the getter method for the given property name in the specified class.
	 * It checks for methods with prefixes "get" and "is".
	 *
	 * @param clazz the class in which to find the getter method.
	 * @param propertyName the name of the property for which the getter method is to be found.
	 * @return the getter method if found; {@code null} otherwise.
	 */
	public static Method findGetterMethod(final @NonNull Class<?> clazz, final @NonNull String propertyName) {
		return findMethod(clazz, propertyName, new String[] { "get", "is" });
	}

	/**
	 * Finds and returns the setter method for the given property name in the specified class.
	 *
	 * @param clazz the class in which to find the setter method.
	 * @param propertyName the name of the property for which the setter method is to be found.
	 * @param paramTypes the parameter types of the setter method.
	 * @return the setter method if found; {@code null} otherwise.
	 */
	public static Method findSetterMethod(final @NonNull Class<?> clazz, final @NonNull String propertyName, final @NonNull Class<?>... paramTypes) {
		return findMethod(clazz, propertyName, new String[] { "set" }, paramTypes);
	}

	/**
	 * Finds and returns the method for the given property name in the specified class using the given prefixes.
	 *
	 * @param clazz the class in which to find the method.
	 * @param propertyName the name of the property for which the method is to be found.
	 * @param methodPrefixes the array of prefixes to use when searching for the method.
	 *            If empty, the method searches for a method matching the property name directly.
	 * @param paramTypes the parameter types of the method.
	 * @return the method if found with the given prefixes and matching parameter types; {@code null} otherwise.
	 */
	public static Method findMethod(final @NonNull Class<?> clazz, final @NonNull String propertyName, final @NonNull String[] methodPrefixes, final @NonNull Class<?>... paramTypes) {
		if (methodPrefixes.length == 0) {
			try {
				return clazz.getMethod(propertyName, paramTypes);
			} catch (final @SuppressWarnings("unused") NoSuchMethodException ex) {
				return null;
			}
		}
		final String standardPropertyName = propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
		for (final String methodPrefix : methodPrefixes) {
			try {
				return clazz.getMethod(methodPrefix + standardPropertyName, paramTypes);
			} catch (final @SuppressWarnings("unused") NoSuchMethodException ex) {
				// keep trying ...
			}
		}
		return null;
	}

	private ReflectionUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
