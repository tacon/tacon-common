package dev.tacon.common.lang;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import dev.tacon.annotations.NonNullByDefault;

/**
 * Utility class that provides various functionalities related to {@link Class} objects.
 * This class allows operations like fetching primitive types, their corresponding wrappers,
 * and other class hierarchy related functionalities.
 */
@NonNullByDefault
public final class ClassUtils {

	private static final Map<Class<?>, Class<?>> PRIMITIVES = Map.of(
			Boolean.class, Boolean.TYPE,
			Byte.class, Byte.TYPE,
			Character.class, Character.TYPE,
			Double.class, Double.TYPE,
			Float.class, Float.TYPE,
			Integer.class, Integer.TYPE,
			Long.class, Long.TYPE,
			Short.class, Short.TYPE,
			Void.class, Void.TYPE);

	private static final Map<Class<?>, Class<?>> WRAPPERS = Map.of(
			Boolean.TYPE, Boolean.class,
			Byte.TYPE, Byte.class,
			Character.TYPE, Character.class,
			Double.TYPE, Double.class,
			Float.TYPE, Float.class,
			Integer.TYPE, Integer.class,
			Long.TYPE, Long.class,
			Short.TYPE, Short.class,
			Void.TYPE, Void.class);

	/**
	 * Casts the given class to a target class type without type checking.
	 *
	 * @param <T> The target type.
	 * @param c the class to be cast.
	 * @return the class cast to the target type.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> uncheckedCast(final Class<?> c) {
		return (Class<T>) c;
	}

	/**
	 * Checks if a given class is a primitive numeric type.
	 *
	 * @param c The class to check.
	 * @return True if the class represents a primitive number, false otherwise.
	 */
	public static boolean isPrimitiveNumber(final Class<?> c) {
		return c.isPrimitive()
				&& c != char.class
				&& c != boolean.class
				&& c != void.class;
	}

	/**
	 * Retrieves the primitive type for a given wrapper class.
	 *
	 * @param c the wrapper class.
	 * @return the corresponding primitive type for the provided wrapper,
	 *         or {@code null} if not found.
	 */
	public static Class<?> getPrimitive(final Class<?> c) {
		return PRIMITIVES.get(Objects.requireNonNull(c));
	}

	/**
	 * Retrieves the primitive type for a given wrapper class.
	 *
	 * @param c the wrapper class.
	 * @return the corresponding primitive type for the provided wrapper,
	 *         or {@code null} if not found.
	 */
	public static Class<?> getWrapper(final Class<?> c) {
		return WRAPPERS.get(Objects.requireNonNull(c));
	}

	/**
	 * Fetches all the superclasses for a given class.
	 *
	 * @param c the class for which to get the superclasses.
	 * @return A list containing all the superclasses of the provided class.
	 */
	public static List<Class<?>> getSuperclasses(final Class<?> c) {
		Class<?> cls = c.getSuperclass();
		final List<Class<?>> result = new ArrayList<>();
		while (cls != null) {
			result.add(cls);
			cls = cls.getSuperclass();
		}
		return result;
	}

	/**
	 * Fetches all the interfaces implemented by a given class.
	 *
	 * @param c the class for which to get the implemented interfaces.
	 * @return a set containing all the interfaces implemented by the provided class.
	 */
	public static Set<Class<?>> getInterfaces(final Class<?> c) {
		Objects.requireNonNull(c);
		final Set<Class<?>> result = new HashSet<>();
		addInterfaces(c, result);
		return result;
	}

	private static void addInterfaces(final Class<?> c, final Set<Class<?>> result) {
		Class<?> cls = c;
		while (cls != null) {
			for (final Class<?> iface : cls.getInterfaces()) {
				if (!result.contains(iface)) {
					result.add(iface);
					addInterfaces(iface, result);
				}
			}
			cls = cls.getSuperclass();
		}
	}

	private ClassUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
