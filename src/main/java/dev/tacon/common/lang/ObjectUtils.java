package dev.tacon.common.lang;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;

import dev.tacon.annotations.NonNullByDefault;
import dev.tacon.annotations.Nullable;
import dev.tacon.common.lang.reflect.ReflectionUtils;

/**
 * Utility class for operations related to objects and beans.
 * <p>
 * This class provides methods to safely handle operations on objects,
 * including copying bean values, comparing objects, and more.
 * All methods within this class are designed to be null-safe.
 * </p>
 */
@NonNullByDefault
public final class ObjectUtils {

	/**
	 * Copies property values from the source bean to the target bean of the same type.
	 *
	 * @param <T> The type of the beans
	 * @param source the source bean
	 * @param target the target bean
	 * @return the target bean with copied values
	 */
	public static <T> T copyBeanValues(final T source, final T target) {
		final Class<?> origClass = source.getClass();
		final Method[] methods = target.getClass().getMethods();
		for (final Method method : methods) {
			final String methodName = method.getName();
			if (Modifier.isPublic(method.getModifiers())
					&& methodName.startsWith("set")
					&& method.getParameterCount() == 1) {
				final String fieldName = methodName.substring(3);
				final Method m = ReflectionUtils.findGetterMethod(origClass, fieldName);
				if (m != null && Modifier.isPublic(m.getModifiers())) {
					try {
						method.invoke(target, m.invoke(source));
					} catch (@SuppressWarnings("unused") final ReflectiveOperationException ex) {}
				}
			}
		}
		return target;
	}

	/**
	 * Copies property values from the source bean to the target bean based on a specified bean class.
	 *
	 * @param <T> The base type of the beans
	 * @param <T1> The derived type of the beans
	 * @param beanClass the class representing the bean type
	 * @param source the source bean
	 * @param target the target bean
	 * @return the target bean with copied values
	 */
	public static <T, T1 extends T> T1 copyBeanValues(final Class<T> beanClass, final T1 source, final T1 target) {
		Objects.requireNonNull(beanClass);
		Objects.requireNonNull(source);
		final Method[] methods = target.getClass().getMethods();
		for (final Method method : methods) {
			final String methodName = method.getName();
			if (Modifier.isPublic(method.getModifiers())
					&& methodName.startsWith("set")
					&& method.getParameterCount() == 1) {
				final String fieldName = methodName.substring(3);
				final Method m = ReflectionUtils.findGetterMethod(beanClass, fieldName);
				if (m != null && Modifier.isPublic(m.getModifiers())) {
					try {
						method.invoke(target, m.invoke(source));
					} catch (@SuppressWarnings("unused") final ReflectiveOperationException ex) {}
				}
			}
		}
		return target;
	}

	/**
	 * Compares two objects for equality or, if they are both {@code Comparable}, by their comparison result.
	 *
	 * @param <T> The base type of the first object
	 * @param <T1> The derived type of the second object
	 * @param o1 the first object
	 * @param o2 the second object
	 * @return {@code true} if objects are equal or if both are {@code Comparable}
	 *         and have the same comparison value, {@code false} otherwise
	 */
	@SuppressWarnings("all")
	public static <T, T1 extends T> boolean compareOrEqual(final @Nullable T o1, final @Nullable T1 o2) {
		if (o1 instanceof final Comparable c1 && o2 instanceof final Comparable c2) {
			if (c1.compareTo(c2) != 0) {
				return false;
			}
		} else if (!Objects.equals(o1, o2)) {
			return false;
		}
		return true;
	}

	/**
	 * Compares two beans based on their getter methods' values.
	 *
	 * @param obj1 the first bean object
	 * @param obj2 the second bean object
	 * @return {@code true} if all getter methods return equal values, {@code false} otherwise
	 * @see #compareOrEqual(Object, Object)
	 */
	public static boolean compareOrEqualBeans(final @Nullable Object obj1, final @Nullable Object obj2) {
		if (obj1 == obj2) {
			return true;
		}
		if (obj1 == null || obj2 == null) {
			return false;
		}
		final Class<?> clazz = obj1.getClass();
		final Method[] methods = clazz.getMethods();
		for (final Method method : methods) {
			final String methodName = method.getName();
			if (Modifier.isPublic(method.getModifiers()) && method.getParameterTypes().length == 0) {
				if (methodName.startsWith("get") || methodName.startsWith("is")) {
					try {
						if (!compareOrEqual(method.invoke(obj1), method.invoke(obj2))) {
							return false;
						}
					} catch (@SuppressWarnings("unused") final ReflectiveOperationException ex) {}
				}
			}
		}
		return true;
	}

	private ObjectUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
