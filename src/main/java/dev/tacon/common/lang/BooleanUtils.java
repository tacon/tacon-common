package dev.tacon.common.lang;

/**
 * Utility class for performing null-safe operations on {@link Boolean} objects.
 *
 * <p>This class provides a set of methods to facilitate common operations on
 * Boolean objects, ensuring that they are handled in a manner that gracefully
 * accounts for {@code null} values.</p>
 *
 * <p>Typical usage involves operations that would otherwise throw
 * {@link NullPointerException} when dealing with null Boolean references.</p>
 */
public final class BooleanUtils {

	/**
	 * Computes the logical negation of the given {@link Boolean} object.
	 *
	 * <p>For a non-null input value, this method returns its logical negation:
	 * if the input is {@link Boolean#TRUE}, the return value is {@link Boolean#FALSE},
	 * and vice versa. If the provided Boolean object is {@code null}, the method
	 * returns {@code null}.</p>
	 *
	 * @param value the {@link Boolean} object whose logical negation is to be computed
	 * @return the logical negation of the given Boolean, or {@code null} if the input is {@code null}
	 */
	public static Boolean not(final Boolean value) {
		return value == null ? null : value.booleanValue() ? Boolean.FALSE : Boolean.TRUE;
	}

	/**
	 * Determines if the provided {@link Boolean} value is {@code true}.
	 *
	 * <p>This method returns {@code true} if and only if the supplied Boolean object is
	 * non-null and represents the {@code true} value. If the Boolean object is {@code null}
	 * or represents the {@code false} value, this method returns {@code false}.</p>
	 *
	 * <pre>
	 * isTrue(null) -> false
	 * isTrue(Boolean.FALSE) -> false
	 * isTrue(Boolean.TRUE) -> true
	 * </pre>
	 *
	 *
	 * @param value the {@link Boolean} object to be checked
	 * @return {@code true} if the provided Boolean is non-null and true, {@code false} otherwise
	 */
	public static boolean isTrue(final Boolean value) {
		return value != null && value.booleanValue();
	}

	/**
	 * Determines if the provided {@link Boolean} value is {@code false}.
	 *
	 * <p>This method returns {@code true} if and only if the supplied Boolean object is
	 * non-null and represents the {@code false} value. If the Boolean object is {@code null}
	 * or represents the {@code true} value, this method returns {@code false}.</p>
	 *
	 * <pre>
	 * isFalse(null) -> false
	 * isFalse(Boolean.FALSE) -> true
	 * isFalse(Boolean.TRUE) -> false
	 * </pre>
	 *
	 *
	 * @param value the {@link Boolean} object to be checked
	 * @return {@code true} if the provided Boolean is non-null and false, {@code false} otherwise
	 */
	public static boolean isFalse(final Boolean value) {
		return value != null && !value.booleanValue();
	}

	/**
	 * Performs a logical AND operation between two {@link Boolean} objects.
	 *
	 * <p>This method returns {@code true} only if both provided Boolean objects are
	 * non-null and represent the {@code true} value. If either (or both) of the Boolean
	 * objects are {@code null} or represent the {@code false} value, this method returns
	 * {@code false}.</p>
	 *
	 * <p>Usage example:</p>
	 *
	 * <pre>
	 * and(Boolean.TRUE, Boolean.TRUE) -> true
	 * and(Boolean.TRUE, Boolean.FALSE) -> false
	 * and(Boolean.FALSE, Boolean.FALSE) -> false
	 * and(Boolean.TRUE, null) -> false
	 * and(null, null) -> false
	 * </pre>
	 *
	 * @param b1 the first {@link Boolean} object to be evaluated
	 * @param b2 the second {@link Boolean} object to be evaluated
	 * @return {@code true} if both provided Booleans are non-null and true, {@code false} otherwise
	 */
	public static boolean and(final Boolean b1, final Boolean b2) {
		return isTrue(b1) && isTrue(b2);
	}

	/**
	 * Performs a logical OR operation between two {@link Boolean} objects.
	 *
	 * <p>This method returns {@code true} only if any provided Boolean objects are
	 * non-null and represent the {@code true} value. If all of the Boolean
	 * objects are {@code null} or represent the {@code false} value, this method returns
	 * {@code false}.</p>
	 *
	 * <p>Usage example:</p>
	 *
	 * <pre>
	 * or(Boolean.TRUE, Boolean.TRUE) -> true
	 * or(Boolean.TRUE, Boolean.FALSE) -> true
	 * or(Boolean.FALSE, Boolean.FALSE) -> false
	 * or(Boolean.TRUE, null) -> true
	 * or(null, null) -> false
	 * </pre>
	 *
	 * @param b1 the first {@link Boolean} object to be evaluated
	 * @param b2 the second {@link Boolean} object to be evaluated
	 * @return {@code true} if any provided Booleans are non-null and true, {@code false} otherwise
	 */
	public static boolean or(final Boolean b1, final Boolean b2) {
		return isTrue(b1) || isTrue(b2);
	}

	private BooleanUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
