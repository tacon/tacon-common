package dev.tacon.common.lang;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;

/**
 * Utility class for handling and manipulating numbers in various formats.
 */
public final class NumberUtils {

	/**
	 * Common number patterns for formatting.
	 */
	public enum NumberPattern {

		INT("0"),
		INT_THOUSANDS_SEPARATOR("#,##0"),
		INT_PERCENTAGE("0 '%'"),
		INT_PERCENTAGE_100("0 %"),
		TWO_DIGITS("0.00"),
		TWO_DIGITS_THOUSANDS_SEPARATOR("#,##0.00"),
		TWO_DIGITS_PERCENTAGE("0.00 '%'"),
		TWO_DIGITS_PERCENTAGE_100("0.00 %");

		private final DecimalFormat formatter;
		private final String pattern;

		/**
		 * Constructor for NumberPattern.
		 *
		 * @param pattern Pattern string.
		 */
		NumberPattern(final String pattern) {
			this.pattern = pattern;
			this.formatter = new DecimalFormat(pattern);
		}

		/**
		 * Creates a new DecimalFormat instance with this pattern.
		 *
		 * @return The DecimalFormat instance.
		 */
		public DecimalFormat createFormatter() {
			return new DecimalFormat(this.pattern);
		}

		/**
		 * Gets the pattern string.
		 *
		 * @return The pattern string.
		 */
		public String getPattern() {
			return this.pattern;
		}
	}

	public static final Integer INTEGER_ZERO = Integer.valueOf(0);
	public static final Short SHORT_ZERO = Short.valueOf((short) 0);
	public static final Long LONG_ZERO = Long.valueOf(0);
	public static final Float FLOAT_ZERO = Float.valueOf(0);
	public static final Double DOUBLE_ZERO = Double.valueOf(0);
	public static final BigDecimal HUNDRED = BigDecimal.valueOf(100);
	public static final BigInteger HUNDRED_BI = BigInteger.valueOf(100);
	public static final BigDecimal THOUSAND = BigDecimal.valueOf(1000);
	public static final BigInteger THOUSAND_BI = BigInteger.valueOf(1000);

	/**
	 * Converts the given Number to a Byte.
	 *
	 * @param n the number to convert, can be null
	 * @return the converted Byte or null if the input is null
	 */

	public static Byte toByte(final @Nullable Number n) {
		return n == null ? null : Byte.valueOf(n.byteValue());
	}

	/**
	 * Converts the given Number to a Byte with a default value.
	 *
	 * @param n the number to convert, can be null
	 * @param defaultValue the default value to return if the input is null
	 * @return the converted Byte or the default value if the input is null
	 */
	public static @NonNull Byte toByte(final @Nullable Number n, final byte defaultValue) {
		return Byte.valueOf(n == null ? defaultValue : n.byteValue());
	}

	/**
	 * Converts the given Number to a Short.
	 *
	 * @param n the number to convert, can be null
	 * @return the converted Short or null if the input is null
	 */
	public static Short toShort(final @Nullable Number n) {
		return n == null ? null : Short.valueOf(n.shortValue());
	}

	/**
	 * Converts the given Number to a Short with a default value.
	 *
	 * @param n the number to convert, can be null
	 * @param defaultValue the default value to return if the input is null
	 * @return the converted Short or the default value if the input is null
	 */
	public static @NonNull Short toShort(final @Nullable Number n, final short defaultValue) {
		return Short.valueOf(n == null ? defaultValue : n.shortValue());
	}

	/**
	 * Converts the given Number to a Integer.
	 *
	 * @param n the number to convert, can be null
	 * @return the converted Integer or null if the input is null
	 */
	public static Integer toInteger(final @Nullable Number n) {
		return n == null ? null : Integer.valueOf(n.intValue());
	}

	/**
	 * Converts the given Number to a Integer with a default value.
	 *
	 * @param n the number to convert, can be null
	 * @param defaultValue the default value to return if the input is null
	 * @return the converted Integer or the default value if the input is null
	 */
	public static @NonNull Integer toInteger(final @Nullable Number n, final int defaultValue) {
		return Integer.valueOf(n == null ? defaultValue : n.intValue());
	}

	/**
	 * Converts the given Number to a Long.
	 *
	 * @param n the number to convert, can be null
	 * @return the converted Long or null if the input is null
	 */
	public static Long toLong(final @Nullable Number n) {
		return n == null ? null : Long.valueOf(n.longValue());
	}

	/**
	 * Converts the given Number to a Long with a default value.
	 *
	 * @param n the number to convert, can be null
	 * @param defaultValue the default value to return if the input is null
	 * @return the converted Long or the default value if the input is null
	 */
	public static @NonNull Long toLong(final @Nullable Number n, final long defaultValue) {
		return Long.valueOf(n == null ? defaultValue : n.longValue());
	}

	/**
	 * Converts the given Number to a Float.
	 *
	 * @param n the number to convert, can be null
	 * @return the converted Float or null if the input is null
	 */
	public static Float toFloat(final @Nullable Number n) {
		return n == null ? null : Float.valueOf(n.floatValue());
	}

	/**
	 * Converts the given Number to a Float with a default value.
	 *
	 * @param n the number to convert, can be null
	 * @param defaultValue the default value to return if the input is null
	 * @return the converted Float or the default value if the input is null
	 */
	public static @NonNull Float toFloat(final @Nullable Number n, final float defaultValue) {
		return Float.valueOf(n == null ? defaultValue : n.floatValue());
	}

	/**
	 * Converts the given Number to a Double.
	 *
	 * @param n the number to convert, can be null
	 * @return the converted Double or null if the input is null
	 */
	public static Double toDouble(final @Nullable Number n) {
		return n == null ? null : Double.valueOf(n.doubleValue());
	}

	/**
	 * Converts the given Number to a Double with a default value.
	 *
	 * @param n the number to convert, can be null
	 * @param defaultValue the default value to return if the input is null
	 * @return the converted Double or the default value if the input is null
	 */
	public static @NonNull Double toDouble(final @Nullable Number n, final double defaultValue) {
		return Double.valueOf(n == null ? defaultValue : n.doubleValue());
	}

	/**
	 * Converts the given Number to a BigDecimal.
	 *
	 * @param n the number to convert, can be null
	 * @return the converted BigDecimal or null if the input is null
	 */
	public static BigDecimal toBigDecimal(final @Nullable Number n) {
		return toBigDecimal(n, null);
	}

	/**
	 * Converts the given Number to a BigDecimal with a default value.
	 *
	 * @param n the number to convert, can be null
	 * @param defaultValue the default value to return if the input is null
	 * @return the converted BigDecimal or the default value if the input is null
	 */
	public static BigDecimal toBigDecimal(final @Nullable Number n, final BigDecimal defaultValue) {
		return n == null ? defaultValue : n instanceof final BigDecimal bd ? bd : new BigDecimal(n.toString());
	}

	/**
	 * Converts the given Integer to a BigInteger.
	 *
	 * @param n the Integer to convert, can be null
	 * @return the converted BigInteger or null if the input is null
	 */
	public static BigInteger toBigInteger(final @Nullable Integer n) {
		return n == null ? null : BigInteger.valueOf(n.intValue());
	}

	/**
	 * Converts the given Integer to a BigInteger with a default value.
	 *
	 * @param n the Integer to convert, can be null
	 * @param defaultValue the default BigInteger value to return if the input is null
	 * @return the converted BigInteger or the default value if the input is null
	 */
	public static BigInteger toBigInteger(final @Nullable Integer n, final BigInteger defaultValue) {
		return n == null ? defaultValue : BigInteger.valueOf(n.intValue());
	}

	/**
	 * Converts the given Long to a BigInteger.
	 *
	 * @param n the Long to convert, can be null
	 * @return the converted BigInteger or null if the input is null
	 */
	public static BigInteger toBigInteger(final @Nullable Long n) {
		return n == null ? null : BigInteger.valueOf(n.longValue());
	}

	/**
	 * Converts the given Double to a BigInteger.
	 * Note: This conversion uses the longValue of the Double which may result in loss of precision.
	 *
	 * @param n the Double to convert, can be null
	 * @return the converted BigInteger or null if the input is null
	 */
	public static BigInteger toBigInteger(final @Nullable Double n) {
		return n == null ? null : BigInteger.valueOf(n.longValue());
	}

	/**
	 * Converts the given Integer to a BigDecimal with a default null value.
	 *
	 * @param n the Integer to convert, can be null
	 * @param nullValue the BigDecimal value to return if the input is null
	 * @return the converted BigDecimal or the nullValue if the input is null
	 */
	public static BigDecimal toBigDecimal(final @Nullable Integer n, final @Nullable BigDecimal nullValue) {
		return n == null ? nullValue : BigDecimal.valueOf(n.intValue());
	}

	/**
	 * Converts the given Integer to a BigDecimal.
	 *
	 * @param n the Integer to convert, can be null
	 * @return the converted BigDecimal or null if the input is null
	 */
	public static BigDecimal toBigDecimal(final @Nullable Integer n) {
		return toBigDecimal(n, null);
	}

	/**
	 * Converts the given Double to a BigDecimal with a default null value.
	 *
	 * @param n the Double to convert, can be null
	 * @param nullValue the BigDecimal value to return if the input is null
	 * @return the converted BigDecimal or the nullValue if the input is null
	 */
	public static BigDecimal toBigDecimal(final @Nullable Double n, final @Nullable BigDecimal nullValue) {
		return n == null ? nullValue : BigDecimal.valueOf(n.doubleValue());
	}

	/**
	 * Converts the given Double to a BigDecimal.
	 *
	 * @param n the Double to convert, can be null
	 * @return the converted BigDecimal or null if the input is null
	 */
	public static BigDecimal toBigDecimal(final @Nullable Double n) {
		return toBigDecimal(n, null);
	}

	// conversion primitive to obj
	/**
	 * Converts the given double value to an Integer.
	 *
	 * @param value the double value to be converted
	 * @return the resulting Integer
	 */
	public static @NonNull Integer toInteger(final double value) {
		return Integer.valueOf((int) value);
	}

	/**
	 * Converts the given double value to a Long.
	 *
	 * @param value the double value to be converted
	 * @return the resulting Long
	 */
	public static @NonNull Long toLong(final double value) {
		return Long.valueOf((long) value);
	}

	// parsing
	/**
	 * Parses the provided string into an Integer. Returns null if the string is null or empty.
	 *
	 * @param s the string to be parsed
	 * @return the parsed Integer or null
	 */
	public static Integer parseInteger(final @Nullable String s) {
		return parseInteger(s, null);
	}

	/**
	 * Parses the provided string into an Integer. Returns the provided default value if parsing fails.
	 *
	 * @param s the string to be parsed
	 * @param defaultValue the default value to return if parsing fails
	 * @return the parsed Integer or the default value
	 */
	public static Integer parseInteger(final String s, final int defaultValue) {
		return parseInteger(s, Integer.valueOf(defaultValue));
	}

	/**
	 * Parses the provided string into an Integer. Returns the provided default value or null if parsing fails.
	 *
	 * @param s the string to be parsed, can be null
	 * @param defaultValue the default value to return if parsing fails, can be null
	 * @return the parsed Integer or the default value or null
	 */
	public static Integer parseInteger(final @Nullable String s, final @Nullable Integer defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Integer.valueOf(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Parses the provided string into a primitive int. Returns the provided default value if parsing fails.
	 *
	 * @param s the string to be parsed, can be null
	 * @param defaultValue the default value to return if parsing fails
	 * @return the parsed int value or the default value
	 */
	public static int parseInt(final @Nullable String s, final int defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Integer.parseInt(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Parses the provided string into a Short. Returns null if the string is null or empty.
	 *
	 * @param s the string to be parsed
	 * @return the parsed Short or null
	 */
	public static Short parseShort(final @Nullable String s) {
		return parseShort(s, null);
	}

	/**
	 * Parses the provided string into a Short. Returns the provided default value if parsing fails.
	 *
	 * @param s the string to be parsed
	 * @param defaultValue the default value to return if parsing fails
	 * @return the parsed Short or the default value
	 */
	public static Short parseShort(final String s, final short defaultValue) {
		return parseShort(s, Short.valueOf(defaultValue));
	}

	/**
	 * Parses the provided string into a Short. Returns the provided default value or null if parsing fails.
	 *
	 * @param s the string to be parsed, can be null
	 * @param defaultValue the default value to return if parsing fails, can be null
	 * @return the parsed Short or the default value or null
	 */
	public static Short parseShort(final @Nullable String s, final @Nullable Short defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Short.valueOf(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Parses the provided string into a Long. Returns null if the string is null or empty.
	 *
	 * @param s the string to be parsed
	 * @return the parsed Long or null
	 */
	public static Long parseLong(final @Nullable String s) {
		return parseLong(s, null);
	}

	/**
	 * Parses the provided string into a Long. Returns the provided default value if parsing fails.
	 *
	 * @param s the string to be parsed
	 * @param defaultValue the default value to return if parsing fails
	 * @return the parsed Long or the default value
	 */
	public static Long parseLong(final String s, final long defaultValue) {
		return parseLong(s, Long.valueOf(defaultValue));
	}

	/**
	 * Parses the provided string into a Long. Returns the provided default value or null if parsing fails.
	 *
	 * @param s the string to be parsed, can be null
	 * @param defaultValue the default value to return if parsing fails, can be null
	 * @return the parsed Long or the default value or null
	 */
	public static Long parseLong(final @Nullable String s, final @Nullable Long defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Long.valueOf(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Parses the provided string into a Double. Returns null if the string is null or empty.
	 *
	 * @param s the string to be parsed
	 * @return the parsed Double or null
	 */
	public static Double parseDouble(final String s) {
		return parseDouble(s, null);
	}

	/**
	 * Parses the provided string into a Double. Returns the provided default value if parsing fails.
	 *
	 * @param s the string to be parsed
	 * @param defaultValue the default value to return if parsing fails
	 * @return the parsed Double or the default value
	 */
	public static Double parseDouble(final String s, final double defaultValue) {
		return parseDouble(s, Double.valueOf(defaultValue));
	}

	/**
	 * Parses the provided string into a Double. Returns the provided default value or null if parsing fails.
	 *
	 * @param s the string to be parsed, can be null
	 * @param defaultValue the default value to return if parsing fails, can be null
	 * @return the parsed Double or the default value or null
	 */
	public static Double parseDouble(final String s, final Double defaultValue) {
		if (s != null && !s.isEmpty()) {
			try {
				return Double.valueOf(s);
			} catch (final @SuppressWarnings("unused") NumberFormatException nfex) {
				//
			}
		}
		return defaultValue;
	}

	/**
	 * Converts a zero Integer to null, or returns the original value if it's not zero.
	 *
	 * @param value the Integer value to check
	 * @return the original value if it's not zero, or null otherwise
	 */
	public static Integer zeroToNull(final @Nullable Integer value) {
		return INTEGER_ZERO.equals(value) ? null : value;
	}

	/**
	 * Converts a zero or null BigDecimal to null, or returns the original value if it's not zero.
	 *
	 * @param value the BigDecimal value to check
	 * @return the original value if it's not zero, or null otherwise
	 */
	public static BigDecimal zeroToNull(final @Nullable BigDecimal value) {
		return value == null || value.signum() == 0 ? null : value;
	}

	/**
	 * Checks if a given Number is positive or zero.
	 *
	 * @param n the Number to check
	 * @return true if the Number is positive or zero, false otherwise
	 */
	public static boolean isPositiveOrZero(final @Nullable Number n) {
		if (n == null) {
			return false;
		}
		if (n instanceof BigDecimal) {
			return ((BigDecimal) n).signum() >= 0;
		}
		return n.doubleValue() >= 0;
	}

	/**
	 * Checks if a given Number is positive.
	 *
	 * @param n the Number to check
	 * @return true if the Number is positive, false otherwise
	 */
	public static boolean isPositive(final Number n) {
		if (n == null) {
			return false;
		}
		if (n instanceof BigDecimal) {
			return ((BigDecimal) n).signum() > 0;
		}
		return n.doubleValue() > 0;
	}

	/**
	 * Checks if a given Number is negative.
	 *
	 * @param n the Number to check
	 * @return true if the Number is negative, false otherwise
	 */
	public static boolean isNegative(final Number n) {
		if (n == null) {
			return false;
		}
		if (n instanceof BigDecimal) {
			return ((BigDecimal) n).signum() < 0;
		}
		return n.doubleValue() < 0;
	}

	// rounding
	/**
	 * Rounds a given double value to the specified number of decimals using the HALF_UP rounding mode.
	 *
	 * @param num the number to round
	 * @param decimals the number of decimals
	 * @return the rounded value
	 */
	public static double round(final double num, final int decimals) {
		return round(num, decimals, RoundingMode.HALF_UP);
	}

	/**
	 * Rounds a given double value to the specified number of decimals using the provided rounding mode.
	 *
	 * @param num the number to round
	 * @param decimals the number of decimals
	 * @param roundingMode the rounding mode to use
	 * @return the rounded value
	 */
	public static double round(final double num, final int decimals, final @NonNull RoundingMode roundingMode) {
		if (Double.isInfinite(num) || Double.isNaN(num)) {
			/*
			 * devo controllare se il numero è infinito o "not a number"
			 * per non far saltare il costruttore BigDecimal
			 */
			return num;
		}
		return new BigDecimal(num).setScale(decimals, roundingMode).doubleValue();
	}

	/**
	 * Rounds a Double value to the specified number of decimals using the provided rounding mode.
	 *
	 * @param num the Double number to round, can be null
	 * @param decimals the number of decimals
	 * @param tipo the rounding mode to use
	 * @return the rounded Double or null
	 */
	public static Double round(final Double num, final int decimals, final @NonNull RoundingMode tipo) {
		if (num == null || num.isInfinite() || num.isNaN()) {
			return num;
		}
		return Double.valueOf(round(num.doubleValue(), decimals, tipo));
	}

	// format
	/**
	 * Formats a given double value to a string with the specified number of decimals.
	 *
	 * @param value the number to format
	 * @param decimals the number of decimals
	 * @return the formatted string
	 */
	@NonNull
	public static String format(final double value, final int decimals) {
		return createPattern(decimals).format(value);
	}

	/**
	 * Creates a DecimalFormat pattern with the specified number of decimals.
	 *
	 * @param decimals the number of decimals
	 * @return the formatted pattern
	 */
	public static DecimalFormat createPattern(final int decimals) {
		if (decimals < 0) {
			throw new IllegalArgumentException("Number of decimals must be positive");
		}
		final DecimalFormat decimalFormat = new DecimalFormat();
		decimalFormat.setMaximumFractionDigits(decimals);
		decimalFormat.setMinimumFractionDigits(decimals);
		return decimalFormat;
	}

	/**
	 * Formats the given int value using a specific NumberPattern.
	 *
	 * @param value the integer value to format
	 * @param pattern the specific pattern to use for formatting
	 * @return the formatted string
	 */
	public static String format(final int value, final NumberPattern pattern) {
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formats the given long value using a specific NumberPattern.
	 *
	 * @param value the long value to format
	 * @param pattern the specific pattern to use for formatting
	 * @return the formatted string
	 */
	public static String format(final long value, final NumberPattern pattern) {
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formats the given float value using a specific NumberPattern.
	 *
	 * @param value the float value to format
	 * @param pattern the specific pattern to use for formatting
	 * @return the formatted string
	 */
	public static String format(final float value, final NumberPattern pattern) {
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formats the given double value using a specific NumberPattern.
	 *
	 * @param value the double value to format
	 * @param pattern the specific pattern to use for formatting
	 * @return the formatted string
	 */
	public static String format(final double value, final NumberPattern pattern) {
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formats the given number value using the specified NumberFormat formatter.
	 *
	 * @param value the number to format, can be null
	 * @param formatter the number format to use for formatting
	 * @return the formatted string or an empty string if the number is null
	 */
	public static String format(final Number value, final NumberPattern pattern) {
		if (value == null) {
			return "";
		}
		synchronized (pattern.formatter) {
			return pattern.formatter.format(value);
		}
	}

	/**
	 * Formats the given Number value using a specified pattern string.
	 *
	 * @param value the Number value to format; if null, an empty string will be returned
	 * @param pattern the string pattern to use for formatting
	 * @return the formatted string based on the given pattern, or an empty string if the value is null
	 */
	public static String format(final Number value, final String pattern) {
		return value == null ? "" : format(value, new DecimalFormat(pattern));
	}

	/**
	 * Formats the given Number value using a specific NumberFormat.
	 *
	 * @param value the Number value to format; if null, an empty string will be returned
	 * @param formatter the NumberFormat instance to use for formatting
	 * @return the formatted string based on the provided formatter, or an empty string if the value is null
	 */
	public static String format(final Number value, final NumberFormat formatter) {
		if (value == null) {
			return "";
		}
		return formatter.format(value);
	}

	/**
	 * Formats a given number value to a string with the specified number of decimals.
	 *
	 * @param value the number to format, can be null
	 * @param decimals the number of decimals
	 * @return the formatted string or an empty string if the number is null
	 */
	public static String format(final Number value, final int decimals) {
		if (value == null) {
			return "";
		}
		final DecimalFormat nf = new DecimalFormat();
		nf.setMaximumFractionDigits(decimals);
		nf.setMinimumFractionDigits(decimals);
		return nf.format(value);
	}

	/**
	 * Parses a given string into a BigDecimal.
	 *
	 * @param value the string representation of a number
	 * @return the parsed BigDecimal or null
	 */
	public static BigDecimal parseBigDecimal(final String value) {
		if (value == null || value.isBlank()) {
			return null;
		}
		final DecimalFormat nf = new DecimalFormat();
		nf.setParseBigDecimal(true);
		try {
			final Number p = nf.parse(value);
			final BigDecimal x = toBigDecimal(p);
			return x;
		} catch (final @SuppressWarnings("unused") ParseException ex) {
			return null;
		}
	}

	private NumberUtils() {
		throw new AssertionError("Not instantiable: " + this.getClass());
	}
}
